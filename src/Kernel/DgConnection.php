<?php

/**
 * Class DgConnection
 */
class dgConnection
{
    /**
     * @var PDO
     */
    private static $pdo = false;
    /**
     * @var int zagnieżdzenie transakcji
     */
    private static $transactionCounter = 0;

    /**
     * @var bool czy włączony jest debugMode
     */
    private static $debugMode = false;

    /**
     * @return boolean
     */
    public static function isDebugMode()
    {
        return self::$debugMode;
    }

    /**
     * @param boolean $debugMode
     */
    public static function setDebugMode($debugMode)
    {
        self::$debugMode = $debugMode;
    }

    /**
     * @return PDO
     */
    public static function getPDO()
    {
        if (self::$pdo == false)
        {
            self::initPDO();
        }
        return self::$pdo;
    }

    /**
     * inicjalizuje PDO, łączy się z bazą danych
     */
    private static function initPDO()
    {
        try
        {
            self::$pdo = new PDO('mysql' . ':host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS);
            self::execute("SET NAMES latin1");
        }
        catch (PDOException $e)
        {
            die("PDO error:".$e->getMessage());
        }
    }

    /**
     * @param $sql
     * @return PDOStatement
     */
    public static function prepare($sql)
    {
        return self::getPDO()->prepare($sql);
    }

    /**
     * @param $sql - zapytanie sql
     * @param $params - parametry
     * @return PDOStatement
     * @throws Exception
     */
    public static function execute($sql, $params = null)
    {
        try
        {
            $q = self::prepare($sql);
            self::debugToFile($sql,$params);
            $q->execute($params);

            if($q->errorCode() != 0) {
                $devMsg = var_export($q->errorInfo(),true)."\n".self::prepareFullQuery($sql, $params);;
                self::saveDebugToFile('db', $devMsg);
                throw new Exception('Błąd zapytania do bazy danych.');
            }
            return $q;
        }
        catch(PDOException $e)
        {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Rozpoczyna transakcję
     * @return bool <b>TRUE</b> on success or <b>FALSE</b> on failure.
     */
    public function beginTransaction()
    {
        if (!self::$transactionCounter++) {
            return self::getPDO()->beginTransaction();
        }
        self::getPDO()->exec('SAVEPOINT trans'.self::$transactionCounter);
        return self::$transactionCounter >= 0;
    }

    /**
     * Potwierdza transakcję
     * @return bool <b>TRUE</b> on success or <b>FALSE</b> on failure.
     */
    public function commit()
    {
        if (!--self::$transactionCounter) {
            return self::getPDO()->commit();
        }
        return self::$transactionCounter >= 0;
    }

    /**
     * Wycofuje transakcję
     * @return bool <b>TRUE</b> on success or <b>FALSE</b> on failure.
     */
    public function rollback()
    {
        if (--self::$transactionCounter) {
            self::getPDO()->exec('ROLLBACK TO trans'.(self::$transactionCounter+1));
            return true;
        }
        return self::getPDO()->rollBack();
    }

    /**
     * Jeżeli debugowanie jest włączone, to zapisuje wykonane zapytanie SQL do pliku *.log
     *
     * @param $sql
     * @param $params (default:array())
     *
     * @internal param bool $debug (default: true)
     */
    private static function debugToFile($sql, $params = array())
    {
        if (self::$debugMode) {
            $debugFileName = 'sql';
            $devMsg = self::prepareFullQuery($sql, $params);
            self::saveDebugToFile($debugFileName, $devMsg);
        }
    }

    /**
     * Umieszczenie danych z tablicy $params w zapytaniu SQL.
     * Pozwala podglądnąć całe zapytanie SQL i skopiować do dalszej analizy.
     *
     * @param String $sql    - zapytanie SQL
     * @param Array  $params - parametry zapytania
     *
     * @return String
     */
    private static function prepareFullQuery($sql, array $params)
    {
        if (count($params) == 0) {
            return $sql;
        }

        foreach ($params as $param) {
            if (is_null($param)) {
                $param = 'NULL';
            } elseif (!is_numeric($param)) {
                $param = "'" . str_replace("'", "''", $param) . "'";
            }
            $sql = preg_replace('#\?#', $param, $sql, 1);
        }

        return $sql;
    }

    /**
     * Zapisanie komunikatu błędu do odpowiedniego pliku w folderze z raportami błędów
     * Po zapisaniu wiadomości nastąpi próba wysłania pliku na serwer logs.madkom.pl
     *
     * @param string $type - 'php', 'exception', 'db', 'js' itp.
     * @param string $devMessage - komunikat błędu do zapisania
     * @param string $md5
     * @param bool $minimize
     * @return string
     */
    private function saveDebugToFile($type, &$devMessage, $md5 = '', $minimize = false)
    {
        $errorFile = strtolower($type) . '.log';

        if($minimize === true) {
            error_log($devMessage . "\n", 3, $errorFile);
            return;
        }

        if(empty($md5)) {
            $md5 = md5($devMessage);
        }

        // Czy błąd się powtórzył?
        if(file_exists($errorFile) && is_writable($errorFile)) {
            $fewBytes = 100;
            $fr = fopen($errorFile, 'r+');
            if($fr) {
                if(fseek($fr, -$fewBytes, SEEK_END) === 0) {
                    $fpos = ftell($fr);
                    $fewLines = '';
                    while (!feof($fr)) {
                        $fewLines .= fgets($fr, $fewBytes);
                    }
                    if(preg_match("#([0-9]+)\t@\t{$md5}#", $fewLines, $matches)) {
                        fseek($fr, $fpos);
                        ftruncate($fr, $fpos + $fewBytes);
                        fputs($fr, str_replace($matches[0], (intval($matches[1]) + 1) . "\t@\t" . $md5, $fewLines));
                        fclose($fr);
                        return;
                    }
                }
            }
        }

        // Jest to nowy błąd
        list($usec, $sec) = explode(' ', microtime());
        $msgHeader = "#^#\t" . date("Y-m-d H:i:s", $sec) . "\t" . bcadd($sec, $usec, 8) . "\t\"{$type}\"\t#^#\n";
        $msgFooter = "\n#$#$#\t" . "1\t@\t" . $md5 . "\t#$#$#" . "\n\n\n\n";
        error_log($msgHeader . substr($devMessage, 0, 10240) . $msgFooter, 3, $errorFile);
    }
}