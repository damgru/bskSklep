<?php

/**
 * Created by PhpStorm.
 * User: CAINON
 * Date: 2016-04-27
 * Time: 11:01
 */
class Settings extends AbstractController
{
    /**
     * Settings constructor.
     */
    public function __construct()
    {
        //$this->route(SITE_DEFAULT_CLASS, SITE_DEFAULT_METHOD);
    }

    public function Show() {
        $view = new DashboardView();
        $view->setLevelSelect('uzytkownik');
        $view->easyStart('Ustawienia');
        $view->addBlock('SETTINGS_CONTENT');
        $view->end();
    }

    public function AddUser() {
        $params = $this->getArgs(array('login', 'haslo', 'imie', 'nazwisko', 'poziom'));
        $params['haslo'] = password_hash($params['haslo'], PASSWORD_DEFAULT);
        $dbUsers = new DbUsers();
        $dbUsers->insert($params);
        $this->route('Settings', 'Show');
    }

    public function EditUser()
    {
        $dbUsers = new DbUsers();
        $view = new DashboardView();
        $user = $dbUsers->getUserById($this->get('id'));
        $view->setVariables($user);
        $view->easyStart("Ustawienia", "Edycja użytkownika");
        $view->setLevelSelect('uzytkownik');
        $view->addBlock('SETTINGS_EDITUSER');
        $view->end();
    }

    public function GetUserAccessTableJson() {
        $db = new DbAccessTable();
        $data = $db->getUserAccessTable($this->get('id'));
        return $this->returnDataTableJson($data);
    }

    public function AddUserAccessTable() {
        $db = new DbAccessTable();
        $db->add($this->get('nazwa_tabeli'),$this->get('login'),$this->get('poziom'));
        $dbUsers = new DbUsers();
        $user = $dbUsers->getUserByName($this->get('login'));
        $this->route('Settings','editUser', array('id' => $user['id']));
    }

    public function GetUsersJson() {
        $db = new DbUsers();
        return $this->returnDataTableJson($db->getUsers());
    }

    public function DeleteUserAccessTable() {
        $db = new DbAccessTable();
        $db->deleteById($this->get('access_id'));
        $this->route('Settings', 'Show');
    }
}