-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Czas generowania: 01 Cze 2016, 13:48
-- Wersja serwera: 10.1.9-MariaDB
-- Wersja PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `bsk_sklep`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `dostep_tabela`
--

CREATE TABLE `dostep_tabela` (
  `id` int(11) NOT NULL,
  `nazwa_tabeli` varchar(50) NOT NULL,
  `uzytkownik_id` int(11) NOT NULL,
  `poziom` int(11) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT 'A'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `dostep_tabela`
--

INSERT INTO `dostep_tabela` (`id`, `nazwa_tabeli`, `uzytkownik_id`, `poziom`, `status`) VALUES
(1, 'klient', 1, 2, 'A'),
(2, 'umowa', 1, 3, 'A'),
(3, 'produkt', 1, 3, 'A'),
(6, 'umowa_ma_produkty', 1, 3, 'A'),
(7, 'informacje_o_produkcie', 1, 3, 'A'),
(8, 'sprzedaz', 1, 2, 'A'),
(9, 'uzytkownik', 2, 0, 'A'),
(10, 'dostep_tabela', 2, 0, 'A'),
(12, 'sprzedaz', 2, 0, 'D'),
(13, 'produkty', 2, 0, 'D'),
(14, 'produkt', 2, 0, 'A'),
(15, 'sprzedaz_ma_produkty', 1, 2, 'A');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `informacje_o_produkcie`
--

CREATE TABLE `informacje_o_produkcie` (
  `id` int(11) NOT NULL,
  `produkt_id` int(11) NOT NULL,
  `tytul` varchar(200) NOT NULL,
  `opis` text NOT NULL,
  `data_powstania` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `uzytkownik_id` int(11) NOT NULL,
  `poziom` int(11) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT 'A'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `informacje_o_produkcie`
--

INSERT INTO `informacje_o_produkcie` (`id`, `produkt_id`, `tytul`, `opis`, `data_powstania`, `uzytkownik_id`, `poziom`, `status`) VALUES
(2, 1, 'kolejna informacja', 'cos ciekawego dla fanow', '2016-05-17 11:02:54', 1, 0, 'A'),
(9, 1, 'test edit', 'test2mqwqrqerqe', '2016-05-17 11:36:26', 1, 3, 'A'),
(10, 3, 'ciekawe info', 'raki zimujÄ…', '2016-05-17 12:04:17', 1, 3, 'D'),
(11, 3, 'test', 'test2', '2016-05-18 08:41:46', 1, 3, 'D'),
(12, 1, 'inne info', 'do usuniecia', '2016-06-01 10:31:01', 1, 3, 'D'),
(13, 3, 'test', ' asdf', '2016-06-01 10:42:04', 1, 3, 'D');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `klient`
--

CREATE TABLE `klient` (
  `id` int(11) NOT NULL,
  `email` varchar(250) NOT NULL,
  `imie` varchar(100) NOT NULL,
  `nazwisko` varchar(100) NOT NULL,
  `nazwa` varchar(150) NOT NULL,
  `poziom` int(11) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT 'A'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `klient`
--

INSERT INTO `klient` (`id`, `email`, `imie`, `nazwisko`, `nazwa`, `poziom`, `status`) VALUES
(1, 'dziwnypan@stock.com', 'Dziwny', 'Pan', 'DziwnyPan', 4, 'A'),
(3, 'tajnypan@rzadowadomena.gov', 'Lech', 'Plata', 'Bolek', 4, 'A'),
(6, 'jkowal@popularnyemail.com', 'Janek', 'Kowalski', 'Jkowal', 2, 'A'),
(8, 'BlankaWozniak@inbound.plus', 'Blanka', 'WoÅºniak', ' Formula Gray', 2, 'A'),
(9, 'asfad@asfa.pl', 'afaf', 'asfad', 'afadf', 0, 'A'),
(10, 'kmur@pechmail.com', 'Krzysiek', 'Murarski', 'brak', 2, 'A');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `produkt`
--

CREATE TABLE `produkt` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(150) NOT NULL,
  `data_wydania` date NOT NULL,
  `autor` varchar(200) NOT NULL,
  `poziom` int(11) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT 'A'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `produkt`
--

INSERT INTO `produkt` (`id`, `nazwa`, `data_wydania`, `autor`, `poziom`, `status`) VALUES
(1, 'Deadpool', '2016-04-20', 'ktostam', 2, 'A'),
(2, 'Wiedzmin 3', '2016-04-20', 'CD Projekt', 0, 'A'),
(3, 'tajny produkt', '2016-05-11', 'Magneto', 3, 'A'),
(4, 'test', '2016-06-01', 'test', 3, 'D'),
(5, 'del', '2016-06-14', 'af', 0, 'D');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `sprzedaz`
--

CREATE TABLE `sprzedaz` (
  `id` int(11) NOT NULL,
  `ilosc` int(11) NOT NULL,
  `cena_za_sztuke` int(11) NOT NULL,
  `umowa_id` int(11) NOT NULL,
  `klient_id` int(11) NOT NULL,
  `poziom` int(11) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT 'A',
  `data` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `sprzedaz`
--

INSERT INTO `sprzedaz` (`id`, `ilosc`, `cena_za_sztuke`, `umowa_id`, `klient_id`, `poziom`, `status`, `data`) VALUES
(1, 0, 0, 10, 6, 0, 'A', '2016-05-17 12:52:11'),
(2, 0, 0, 10, 6, 0, 'A', '2016-05-17 12:52:11'),
(3, 0, 0, 10, 6, 0, 'A', '2016-05-17 12:52:22'),
(4, 0, 0, 10, 6, 2, 'D', '2016-05-18 08:44:57'),
(5, 0, 0, 10, 6, 0, 'A', '2016-05-18 11:14:31'),
(6, 0, 0, 10, 6, 0, 'A', '2016-05-18 11:15:15'),
(7, 0, 0, 0, 6, 2, 'A', '2016-05-18 11:43:41'),
(8, 0, 0, 0, 6, 2, 'A', '2016-05-18 11:44:24'),
(9, 0, 0, 10, 9, 2, 'A', '2016-06-01 11:42:17');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `sprzedaz_ma_produkty`
--

CREATE TABLE `sprzedaz_ma_produkty` (
  `id` int(11) NOT NULL,
  `produkt_id` int(11) NOT NULL,
  `sprzedaz_id` int(11) NOT NULL,
  `ilosc` int(11) NOT NULL,
  `cena_za_sztuke` decimal(15,2) NOT NULL,
  `poziom` int(11) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT 'A'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `sprzedaz_ma_produkty`
--

INSERT INTO `sprzedaz_ma_produkty` (`id`, `produkt_id`, `sprzedaz_id`, `ilosc`, `cena_za_sztuke`, `poziom`, `status`) VALUES
(1, 1, 1, 2, '1.01', 3, 'A'),
(2, 1, 2, 3, '10.00', 0, 'A'),
(3, 1, 3, 1, '4.00', 0, 'A'),
(4, 3, 4, 1, '10000.00', 2, 'A'),
(5, 1, 5, 1, '1.00', 0, 'A'),
(6, 2, 6, 1, '999.00', 2, 'A'),
(7, 3, 7, 1, '1.00', 2, 'A'),
(8, 1, 8, 1, '13.00', 0, 'A'),
(9, 1, 9, 1, '17.00', 2, 'A');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `umowa`
--

CREATE TABLE `umowa` (
  `id` int(11) NOT NULL,
  `numer_umowy` varchar(30) NOT NULL,
  `data_od` date NOT NULL,
  `data_do` date DEFAULT NULL,
  `klient_id` int(11) NOT NULL,
  `poziom` int(11) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT 'A'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `umowa`
--

INSERT INTO `umowa` (`id`, `numer_umowy`, `data_od`, `data_do`, `klient_id`, `poziom`, `status`) VALUES
(1, '1/2016', '2016-01-07', '2016-10-27', 1, 2, 'A'),
(4, '2/2016', '2016-04-05', '2016-04-27', 1, 3, 'A'),
(5, '5/2016a', '2016-04-04', '1970-01-01', 7, 3, 'A'),
(6, 'fasdf', '2016-04-06', '2016-04-27', 6, 4, 'A'),
(9, 'asdf', '2016-05-11', '2016-05-19', 6, 3, 'A'),
(10, '6/2016', '2016-05-09', '1970-01-01', 6, 3, 'A');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `umowa_ma_produkty`
--

CREATE TABLE `umowa_ma_produkty` (
  `id` int(11) NOT NULL,
  `ilosc` int(11) NOT NULL,
  `cena` decimal(10,0) NOT NULL,
  `umowa_id` int(11) NOT NULL,
  `produkt_id` int(11) NOT NULL,
  `uzytkownik_id` int(11) NOT NULL,
  `poziom` int(11) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT 'A'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `umowa_ma_produkty`
--

INSERT INTO `umowa_ma_produkty` (`id`, `ilosc`, `cena`, `umowa_id`, `produkt_id`, `uzytkownik_id`, `poziom`, `status`) VALUES
(1, 1, '1', 9, 1, 1, 3, 'A'),
(2, 4, '1', 9, 1, 1, 4, 'A'),
(3, 2, '9', 5, 1, 1, 3, 'A'),
(4, 1, '5', 5, 2, 1, 3, 'A'),
(5, 1, '1', 10, 1, 1, 3, 'A'),
(6, 1, '1', 10, 2, 1, 3, 'A');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uzytkownik`
--

CREATE TABLE `uzytkownik` (
  `id` int(11) NOT NULL,
  `login` varchar(45) NOT NULL,
  `haslo` varchar(512) NOT NULL,
  `imie` varchar(150) NOT NULL,
  `nazwisko` varchar(150) NOT NULL,
  `poziom` int(11) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT 'A'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `uzytkownik`
--

INSERT INTO `uzytkownik` (`id`, `login`, `haslo`, `imie`, `nazwisko`, `poziom`, `status`) VALUES
(1, 'damian', '$2y$10$CbOOxF3m3qV23jbeFdIJiOiG9WNI3vmGd6AMTg5K.i5Gu.DuaJvR2', 'Damian', 'Grudzien', 0, 'A'),
(2, 'admin', '$2y$10$CbOOxF3m3qV23jbeFdIJiOiG9WNI3vmGd6AMTg5K.i5Gu.DuaJvR2', 'admin', 'wszechmogacy', 0, 'A'),
(3, 'ktostam', '$2y$10$ywidja4gccE7GElyUZP2BeGrg2/Smu/q6vHij4sHGPcqIgu.wKn5O', 'KtoÅ›tam', 'JakiÅ›tam', 0, 'A');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `dostep_tabela`
--
ALTER TABLE `dostep_tabela`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `informacje_o_produkcie`
--
ALTER TABLE `informacje_o_produkcie`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `klient`
--
ALTER TABLE `klient`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `produkt`
--
ALTER TABLE `produkt`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sprzedaz`
--
ALTER TABLE `sprzedaz`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sprzedaz_ma_produkty`
--
ALTER TABLE `sprzedaz_ma_produkty`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `umowa`
--
ALTER TABLE `umowa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `umowa_ma_produkty`
--
ALTER TABLE `umowa_ma_produkty`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `uzytkownik`
--
ALTER TABLE `uzytkownik`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `dostep_tabela`
--
ALTER TABLE `dostep_tabela`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT dla tabeli `informacje_o_produkcie`
--
ALTER TABLE `informacje_o_produkcie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT dla tabeli `klient`
--
ALTER TABLE `klient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT dla tabeli `produkt`
--
ALTER TABLE `produkt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT dla tabeli `sprzedaz`
--
ALTER TABLE `sprzedaz`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT dla tabeli `sprzedaz_ma_produkty`
--
ALTER TABLE `sprzedaz_ma_produkty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT dla tabeli `umowa`
--
ALTER TABLE `umowa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT dla tabeli `umowa_ma_produkty`
--
ALTER TABLE `umowa_ma_produkty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT dla tabeli `uzytkownik`
--
ALTER TABLE `uzytkownik`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
