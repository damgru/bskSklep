<?php

/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 2015-07-25
 * Time: 17:19
 */
class Dashboard extends AbstractController
{
    public function Index()
    {
        $view = new DashboardView();
        $view->render();
    }
}