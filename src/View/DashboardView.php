<?php

/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 2015-07-24
 * Time: 18:56
 */
class DashboardView extends AbstractView
{
    private $sets = array(
        'title' => 'Dashboard',
        'title_description' => '',
        'module_name' => 'Dashboard',
        'method_name' => 'Start',
    );
    /**
     * DashboardView constructor.
     */
    public function __construct()
    {
        $this->readTemplateFromFile('public/dashboard.html');
        $user = DgUser::getUser();
        $this->setArrayVariable('user',$user);
        $this->setUserLevels();
    }

    public function render()
    {
        $this->start();
        $this->end();
    }

    public function start($config = array()) {
        $config = array_merge($this->sets,$config);
        $this->setVariables($config);
        $this->setArrayVariable('args',$_REQUEST);
        $this->addBlock('DASHBOARD_START');
        if(!empty($config['info_ok'])) { $this->addBlock('INFO_OK'); }
        if(!empty($config['info_fail'])) { $this->addBlock('INFO_FAIL'); }
    }

    public function easyStart($title = '', $method_name = '', $addBlock = '') {
        $this->start( array(
            'title' => $title,
            'title_description' => '',
            'module_name' => $title,
            'method_name' => $method_name,
        ));
        if(!empty($addBlock)) {
            $this->addBlock($addBlock);
        }
    }

    public function end() {
        $this->addBlock('DASHBOARD_END');
        $this->generateOutput();
    }
    
    public function setUserLevels() {
        $this->setVariable('VAR_ACCESSTABLE_KLIENT', 'null', true);
        $this->setVariable('VAR_ACCESSTABLE_SPRZEDAZ', 'null', true);
        $this->setVariable('VAR_ACCESSTABLE_SPRZEDAZ_MA_PRODUKTY', 'null', true);
        $this->setVariable('VAR_ACCESSTABLE_UMOWA', 'null', true);
        $this->setVariable('VAR_ACCESSTABLE_UMOWA_MA_PRODUKTY', 'null', true);
        $this->setVariable('VAR_ACCESSTABLE_DOSTEP_TABELA', 'null', true);
        $this->setVariable('VAR_ACCESSTABLE_INFORMACJE_O_PRODUKCIE', 'null', true);
        $this->setVariable('VAR_ACCESSTABLE_PRODUKT', 'null', true);
        $this->setVariable('VAR_ACCESSTABLE_UZYTKOWNIK', 'null', true);
        $rows = DgUser::getAccessLevels();
        foreach ($rows as $tableName=>$level) {
            $this->addBlock('ACCESS_TABLE_ITEM', array('nazwa_tabeli'=>$tableName, 'poziom'=>$level));
            $this->setVariable('VAR_ACCESSTABLE_'.strtoupper($tableName), (string)((int)$level), true);
        }
    }

    public function createLevelSelect($variableName, $min=0, $max=0, $selectName = 'poziom') {
        $html = '<select class="form-control" name="'.$selectName.'">';
        for ($i = $min; $i <= $max ; $i++) {
            if(!isset(DgUser::$levelNames[$i])) DgUser::$levelNames[$i] = null;
            $html .= '<option value="'.$i.'">'.DgUser::$levelNames[$i].'</option>';
        }
        $html .= '</select>';
        $this->setVariable($variableName, $html, true);
    }

    public function setLevelSelect(
        $tableName = null,
        $nameWriteUp = 'DASHBOARD_FORM_LEVEL_WRITE',
        $nameReadDown = 'DASHBOARD_FORM_LEVEL_READ',
        $nameAllLevels = 'DASHBOARD_FORM_LEVEL_ALL'
    ) {
        if($tableName == null) {
            $min = 0;
            $max = 0;
            throw new Exception('Blad w aplikacji');
        }
        else
        {
            $min = 0;
            $max = DgUser::getAccessLevel($tableName);
        }
        $this->createLevelSelect($nameReadDown, $min, $max);

        if($tableName == null) {
            $min = 0;
            $max = 4;
            throw new Exception('blad w aplikacji');
        }
        else
        {
            $min = DgUser::getAccessLevel($tableName);
            $max = 4;
        }
        $this->createLevelSelect($nameWriteUp, $min, $max);

        $min = 0;
        $max = 4;
        $this->createLevelSelect($nameAllLevels,0,4);

    }

}