<?php

/**
 * Created by PhpStorm.
 * User: CAINON
 * Date: 2016-04-03
 * Time: 12:40
 */
class DbCustomers extends AbstractModel
{
    var $tableName = 'klient';
    const STATE_DELETE = 'D';
    
    public function getCustomers()
    {
        $sql = "
            SELECT *
            FROM klient
            WHERE poziom <= ?        
        ";
        
        return $this->getRows($sql,array(
            DgUser::getAccessLevel($this->tableName)
        ));
            
    }

    public function deleteById($id)
    {
        $sql = "UPDATE klient SET status = ? WHERE id = ? AND poziom = ?";
        $this->execute($sql,array(self::STATE_DELETE,$id,DgUser::getAccessLevel($this->tableName)));
    }

}