<?php

/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 2015-07-25
 * Time: 17:19
 */
class Sales extends AbstractController
{
    public function Show()
    {
        $view = new DashboardView();
        $dbp = new DbProducts();
        $dbs = new DbSales();
        $dbc = new DbCustomers();
        $dba = new DbAgreements();
        $dbshp = new DbSalesHasProducts();
        $view->easyStart("Sprzedaż");

        $view->setLevelSelect($dbs->tableName);
        $view->createLevelSelect(
            'PRODUCT_LEVEL_WRITE',
            DgUser::getAccessLevel($dbshp->tableName),
            DgUser::LEVEL_MAX,
            'product_poziom[]'
        );

        $products = $dbp->getAll();
        $view->addBlockForRows('AGREEMENTS_PRODUCTS', $products);

        $customers = $dbc->getCustomers();
        $view->addBlockForRows('CUSTOMER_ITEM', $customers);

        $agreements = $dba->getAll();
        $view->addBlockForRows('AGREEMENT_ITEM', $agreements);

        $view->addBlock('ADD_SALE');
        $view->addBlock('SALES_SHOW');
        $view->end();
    }

    public function Add()
    {
        $dbSales = new DbSales();

        $args = $this->getArgs();
        $params = $this->getArgs(array('umowa_id', 'poziom', 'klient_id'));

        $dbSales->insert($params);
        $sprzedazId = $dbSales->getLastInsertId();

        foreach ($args['product'] as $key=>$productId) {
            $sprzedazMaProdukty = array(
                'produkt_id' => $productId,
                'sprzedaz_id' => $sprzedazId,
                'poziom' => $args['product_poziom'][$key],
                'ilosc' => $args['product_count'][$key],
                'cena_za_sztuke' => $args['product_amount'][$key],
            );
            $dbshp = new DbSalesHasProducts();
            $dbshp->insert($sprzedazMaProdukty);
        }
        $this->route('Sales', 'Show');
    }

    public function GetSummaryDayTableJson()
    {
        $dbs = new DbSales();
        $data = $dbs->getSummaryDayTable();
        return $this->returnDataTableJson($data);
    }

    public function Delete()
    {
        $dbs = new DbSales();
        $dbs->deleteById($this->get('id'));
        $this->route('Sales', 'Show');
    }
    
    public function getJson()
    {
        $dbs = new DbSales();
        $data = $dbs->getAll();
        return $this->returnDataTableJson($data);
    }
}