<?php
/*
 * Class template sk�adana z 2 darmowych znalezionych na necie 
 * dostosowana do php4 i php5 , wczesniej tylko php5
 * by: Redzik
 * TODO:
    - doda� Case
    - timeouty
    - sprawdzi� czy nie ma nic na zabezpieczeniach dostepu do plik�w
 */

/*
 * Poprawki biblioteki: Damian Grudzie� (damgrudz@gmail.com)
 * - poprawione b��dy sk�adni
 * - poprawione liter�wki
 * - dodana metoda setVariables, umo�liwiaj�ca przekazanie tablicy parametr�w
 * - okre�lenie prywantych i publicznych metod
 */

class DgTemplator
{

    private $subtemplateBasePath;

    private $maxNestingLevel = 50;
    private $maxInclTemplateSize = 1000000;
    private $template;
    private $varTab;
    private $varTabCnt;
    private $varNameToNoMap;
    private $varRefTab;
    private $varRefTabCnt;
    private $blockTab;
    private $blockTabCnt;
    private $blockNameToNoMap;
    private $openBlocksTab;
    private $blockInstTab;
    private $blockInstTabCnt;
    private $currentNestingLevel;
    private $templateValid = false;
    private $outputMode;
    private $outputFileHandle;
    private $outputError;
    private $outputString;

    public function readTemplateFromFile($fileName)
    {
        if (!$this->readFileIntoString($fileName, $s)) {
            $this->triggerError("B��d podczas odczytu pliku " . $fileName . ".");
            return false;
        }
        if (!$this->setTemplateString($s)) return false;
        return true;
    }

    public function setTemplateString($templateString)
    {
        $this->templateValid = false;
        $this->template = $templateString;
        if (!$this->parseTemplate()) return false;
        $this->reset();
        $this->templateValid = true;
        return true;
    }

    private function loadSubtemplate($subtemplateName, &$s)
    {
        $subtemplateFileName = $this->combineFileSystemPath($this->subtemplateBasePath, $subtemplateName);
        if (!$this->readFileIntoString($subtemplateFileName, $s)) {
            $this->triggerError("B��d podczas wczytywania sub.template " . $subtemplateFileName . ".");
            return false;
        }
        return true;
    }

    private function parseTemplate()
    {
        $this->initParsing();
        $this->beginMainBlock();
        if (!$this->parseTemplateCommands()) return false;
        $this->endMainBlock();
        if (!$this->checkBlockDefinitionsComplete()) return false;
        if (!$this->parseTemplateVariables()) return false;
        $this->associateVariablesWithBlocks();
        return true;
    }

    private function initParsing()
    {
        $this->varTab = array();
        $this->varTabCnt = 0;
        $this->varNameToNoMap = array();
        $this->varRefTab = array();
        $this->varRefTabCnt = 0;
        $this->blockTab = array();
        $this->blockTabCnt = 0;
        $this->blockNameToNoMap = array();
        $this->openBlocksTab = array();
    }

    private function beginMainBlock()
    {
        $blockNo = 0;
        $this->registerBlock('@@InternalMainBlock@@', $blockNo);
        $bte =& $this->blockTab[$blockNo];
        $bte['tPosBegin'] = 0;
        $bte['tPosContentsBegin'] = 0;
        $bte['nestingLevel'] = 0;
        $bte['parentBlockNo'] = -1;
        $bte['definitionIsOpen'] = true;
        $this->openBlocksTab[0] = $blockNo;
        $this->currentNestingLevel = 1;
    }

    private function endMainBlock()
    {
        $bte =& $this->blockTab[0];
        $bte['tPosContentsEnd'] = strlen($this->template);
        $bte['tPosEnd'] = strlen($this->template);
        $bte['definitionIsOpen'] = false;
        $this->currentNestingLevel -= 1;
    }

    private function parseTemplateCommands()
    {
        $p = 0;
        while (true) {
            $p0 = strpos($this->template, '<!--', $p);
            if ($p0 === false) break;
            $p = strpos($this->template, '-->', $p0);
            if ($p === false) {
                $this->triggerError("B��dny koment HTML w lini $p0.");
                return false;
            }
            $p += 3;
            $cmdL = substr($this->template, $p0 + 4, $p - $p0 - 7);
            if (!$this->processTemplateCommand($cmdL, $p0, $p, $resumeFromStart))
                return false;
            if ($resumeFromStart) $p = $p0;
        }
        return true;
    }

    private function processTemplateCommand($cmdL, $cmdTPosBegin, $cmdTPosEnd, &$resumeFromStart)
    {
        $resumeFromStart = false;
        $p = 0;
        $cmd = '';
        if (!$this->parseWord($cmdL, $p, $cmd)) return true;
        $params = substr($cmdL, $p);
        switch (strtoupper($cmd)) {
            case '$BEGINBLOCK':
            case '$BEGIN':
            case '$START':
                if (!$this->processBeginBlockCmd($params, $cmdTPosBegin, $cmdTPosEnd))
                    return false;
                break;
            case '$ENDBLOCK':
            case '$END':
                if (!$this->processEndBlockCmd($params, $cmdTPosBegin, $cmdTPosEnd))
                    return false;
                break;
            case '$INCLUDEDIR':
                if (!$this->processIncludeDirCmd($params, $cmdTPosBegin, $cmdTPosEnd))
                    return false;
                $resumeFromStart = true;
                break;
            case '$INCLUDE':
                if (!$this->processincludeCmd($params, $cmdTPosBegin, $cmdTPosEnd))
                    return false;
                $resumeFromStart = true;
                break;
            default:
                if ($cmd{0} == '$' && !(strlen($cmd) >= 2 && $cmd{1} == '{')) {
                    $this->triggerError("Nieznana komenda \"$cmd\" w $cmdTPosBegin.");
                    return false;
                }
        }
        return true;
    }

    private function processBeginBlockCmd($parms, $cmdTPosBegin, $cmdTPosEnd)
    {
        $p = 0;
        if (!$this->parseWord($parms, $p, $blockName)) {
            $this->triggerError("Brak nazwy bloku \$BeginBlock w $cmdTPosBegin.");
            return false;
        }
        if (trim(substr($parms, $p)) != '') {
            $this->triggerError("Dodatkowy parametr \$BeginBlock w $cmdTPosBegin.");
            return false;
        }
        $this->registerBlock($blockName, $blockNo);
        $btr =& $this->blockTab[$blockNo];
        $btr['tPosBegin'] = $cmdTPosBegin;
        $btr['tPosContentsBegin'] = $cmdTPosEnd;
        $btr['nestingLevel'] = $this->currentNestingLevel;
        $btr['parentBlockNo'] = $this->openBlocksTab[$this->currentNestingLevel - 1];
        $this->openBlocksTab[$this->currentNestingLevel] = $blockNo;
        $this->currentNestingLevel += 1;
        if ($this->currentNestingLevel > $this->maxNestingLevel) {
            $this->triggerError("Block nesting overflow in template at offset $cmdTPosBegin.");
            return false;
        }
        return true;
    }

    private function processEndBlockCmd($parms, $cmdTPosBegin, $cmdTPosEnd)
    {
        $p = 0;
        if (!$this->parseWord($parms, $p, $blockName)) {
            $this->triggerError("Missing block name in \$EndBlock command in template at offset $cmdTPosBegin.");
            return false;
        }
        if (trim(substr($parms, $p)) != '') {
            $this->triggerError("Extra parameter in \$EndBlock command in template at offset $cmdTPosBegin.");
            return false;
        }
        if (!$this->lookupBlockName($blockName, $blockNo)) {
            $this->triggerError("Undefined block name \"$blockName\" in \$EndBlock command in template at offset $cmdTPosBegin.");
            return false;
        }
        $this->currentNestingLevel -= 1;
        $btr =& $this->blockTab[$blockNo];
        if (!$btr['definitionIsOpen']) {
            $this->triggerError("Multiple \$EndBlock command for block \"$blockName\" in template at offset $cmdTPosBegin.");
            return false;
        }
        if ($btr['nestingLevel'] != $this->currentNestingLevel) {
            $this->triggerError("Block nesting level mismatch at \$EndBlock command for block \"$blockName\" in template at offset $cmdTPosBegin.");
            return false;
        }
        $btr['tPosContentsEnd'] = $cmdTPosBegin;
        $btr['tPosEnd'] = $cmdTPosEnd;
        $btr['definitionIsOpen'] = false;
        return true;
    }

    private function registerBlock($blockName, &$blockNo)
    {
        $blockNo = $this->blockTabCnt++;
        $btr =& $this->blockTab[$blockNo];
        $btr = array();
        $btr['blockName'] = $blockName;
        if (!$this->lookupBlockName($blockName, $btr['nextWithSameName']))
            $btr['nextWithSameName'] = -1;
        $btr['definitionIsOpen'] = true;
        $btr['instances'] = 0;
        $btr['firstBlockInstNo'] = -1;
        $btr['lastBlockInstNo'] = -1;
        $btr['blockVarCnt'] = 0;
        $btr['firstVarRefNo'] = -1;
        $btr['blockVarNoToVarNoMap'] = array();
        $this->blockNameToNoMap[strtoupper($blockName)] = $blockNo;
    }

    private function checkBlockDefinitionsComplete()
    {
        for ($blockNo = 0; $blockNo < $this->blockTabCnt; $blockNo++) {
            $btr =& $this->blockTab[$blockNo];
            if ($btr['definitionIsOpen']) {
                $this->triggerError("Missing \$EndBlock command in template for block " . $btr['blockName'] . ".");
                return false;
            }
        }
        if ($this->currentNestingLevel != 0) {
            $this->triggerError("Block nesting level error at end of template.");
            return false;
        }
        return true;
    }

    private function processIncludeCmd($params, $cmdTPosBegin, $cmdTPosEnd)
    {
        $p = 0;
        if (!$this->parseWordOrQuotedString($params, $p, $subtemplateName)) {
            $this->triggerError("Missing or invalid subtemplate name in \$Include command in template at offset $cmdTPosBegin.");
            return false;
        }
        if (trim(substr($params, $p)) != '') {
            $this->triggerError("Extra parameter in \$include command in template at offset $cmdTPosBegin.");
            return false;
        }
        return $this->insertSubtemplate($subtemplateName, $cmdTPosBegin, $cmdTPosEnd);
    }

    private function processIncludeDirCmd($params, $cmdTPosBegin, $cmdTPosEnd)
    {
        $params = trim($params);
        $dir = glob($params.'*');
        $include_text = '';
        foreach($dir as $filename)
        {
            $include_text .= '<!-- $INCLUDE '.$filename.'-->';
        }
        $this->template = substr($this->template, 0, $cmdTPosBegin) . $include_text . substr($this->template, $cmdTPosEnd);
        return true;
    }

    private function insertSubtemplate($subtemplateName, $tPos1, $tPos2)
    {
        if (strlen($this->template) > $this->maxInclTemplateSize) {
            $this->triggerError("Subtemplate include aborted because the internal template string is longer than $this->maxInclTemplateSize characters.");
            return false;
        }
        if (!$this->loadSubtemplate($subtemplateName, $subtemplate)) return false;
        $this->template = substr($this->template, 0, $tPos1) . $subtemplate . substr($this->template, $tPos2);
        return true;
    }

    private function parseTemplateVariables()
    {
        $p = 0;
        while (true) {
            $p = strpos($this->template, '${', $p);
            if ($p === false) break;
            $p0 = $p;
            $p = strpos($this->template, '}', $p);
            if ($p === false) {
                $this->triggerError("Invalid variable reference in template at offset $p0.");
                return false;
            }
            $p += 1;
            $varName = trim(substr($this->template, $p0 + 2, $p - $p0 - 3));
            if (strlen($varName) == 0) {
                $this->triggerError("Empty variable name in template at offset $p0.");
                return false;
            }
            $this->registerVariableReference($varName, $p0, $p);
        }
        return true;
    }

    private function registerVariableReference($varName, $tPosBegin, $tPosEnd)
    {
        if (!$this->lookupVariableName($varName, $varNo))
            $this->registerVariable($varName, $varNo);
        $varRefNo = $this->varRefTabCnt++;
        $vrtr =& $this->varRefTab[$varRefNo];
        $vrtr = array();
        $vrtr['tPosBegin'] = $tPosBegin;
        $vrtr['tPosEnd'] = $tPosEnd;
        $vrtr['varNo'] = $varNo;
    }

    private function registerVariable($varName, &$varNo)
    {
        $varNo = $this->varTabCnt++;
        $vtr =& $this->varTab[$varNo];
        $vtr = array();
        $vtr['varName'] = $varName;
        $vtr['varValue'] = '';
        $this->varNameToNoMap[strtoupper($varName)] = $varNo;
    }

    private function associateVariablesWithBlocks()
    {
        $varRefNo = 0;
        $activeBlockNo = 0;
        $nextBlockNo = 1;
        while ($varRefNo < $this->varRefTabCnt) {
            $vrtr =& $this->varRefTab[$varRefNo];
            $varRefTPos = $vrtr['tPosBegin'];
            $varNo = $vrtr['varNo'];
            if ($varRefTPos >= $this->blockTab[$activeBlockNo]['tPosEnd']) {
                $activeBlockNo = $this->blockTab[$activeBlockNo]['parentBlockNo'];
                continue;
            }
            if ($nextBlockNo < $this->blockTabCnt) {
                if ($varRefTPos >= $this->blockTab[$nextBlockNo]['tPosBegin']) {
                    $activeBlockNo = $nextBlockNo;
                    $nextBlockNo += 1;
                    continue;
                }
            }
            $btr =& $this->blockTab[$activeBlockNo];
            if ($varRefTPos < $btr['tPosBegin'])
                $this->programLogicError(1);
            $blockVarNo = $btr['blockVarCnt']++;
            $btr['blockVarNoToVarNoMap'][$blockVarNo] = $varNo;
            if ($btr['firstVarRefNo'] == -1)
                $btr['firstVarRefNo'] = $varRefNo;
            $vrtr['blockNo'] = $activeBlockNo;
            $vrtr['blockVarNo'] = $blockVarNo;
            $varRefNo += 1;
        }
    }

    private function reset()
    {
        for ($varNo = 0; $varNo < $this->varTabCnt; $varNo++)
            $this->varTab[$varNo]['varValue'] = '';
        for ($blockNo = 0; $blockNo < $this->blockTabCnt; $blockNo++) {
            $btr =& $this->blockTab[$blockNo];
            $btr['instances'] = 0;
            $btr['firstBlockInstNo'] = -1;
            $btr['lastBlockInstNo'] = -1;
        }
        $this->blockInstTab = array();
        $this->blockInstTabCnt = 0;
    }

    public function setVariable($variableName, $variableValue, $isOptional = false)
    {
        if (!$this->templateValid) {
            $this->triggerError("Template not valid.");
            return false;
        }
        if (!$this->lookupVariableName($variableName, $varNo)) {
            if ($isOptional) return true;
            $this->triggerError("Variable \"$variableName\" not defined in template.");
            return false;
        }
        $this->varTab[$varNo]['varValue'] = $variableValue;
        return true;
    }

    public function setVariableEsc($variableName, $variableValue, $isOptional = false)
    {
        return $this->setVariable($variableName, htmlspecialchars($variableValue, ENT_QUOTES), $isOptional);
    }

    /**
     * @param array $variables - tablica z parametrami np. array('nazwazmiennej'=>'wartosc', 'nazwazmiennej2'=>'wartosc')
     * @param bool|true $isOptional czy wszystkie przekazywane parametry musz� by� wykorzystane w widoku
     */
    public function setVariables(array $variables, $isOptional=true)
    {
        if (!is_array($variables)) {
            $variables = array();
        }
        foreach($variables as $key=>$v)
        {
            $this->setVariable($key,$v,$isOptional);
        }
    }

    public function setVariablesToUpper(array $variables, $isOptional=true)
    {
        if (!is_array($variables)) {
            $variables = array();
        }
        foreach($variables as $key=>$v)
        {
            $this->setVariable(strtoupper($key),$v,$isOptional);
        }
    }

    /**
     * Ustawia nowe zmienne w podany sposob np.
     * $name = 'xyz';
     * $variables = array('foo'=>'bar', 'nick'=>'asdf')
     * ustawione zostana 2 zmienne:
     * xyz[foo] -> bar
     * xyz[nick] -> asdf
     *
     * @param $name
     * @param array $variables
     */
    public function setArrayVariable($name, array $variables)
    {
        foreach ($variables as $key => $value) {
            $newName = $name.'['.$key.']';
            $this->setVariable($newName,$value,true);
        }
    }


    public function variableExists($variableName)
    {
        if (!$this->templateValid) {
            $this->triggerError("Template not valid.");
            return false;
        }
        return $this->lookupVariableName($variableName, $varNo);
    }

    public function addBlock($blockName, $variables = array())
    {
        $this->setVariables($variables);

        if (!$this->templateValid) {
            $this->triggerError("Template not valid.");
            return false;
        }
        if (!$this->lookupBlockName($blockName, $blockNo)) {
            $this->triggerError("Block \"$blockName\" not defined in template.");
            return false;
        }
        while ($blockNo != -1) {
            $this->addBlockByNo($blockNo);
            $blockNo = $this->blockTab[$blockNo]['nextWithSameName'];
        }
        return true;
    }

    public function addBlockForRows($blockName, $arrayOfVariables) {
        foreach($arrayOfVariables as $variables) {
            $this->addBlock($blockName,$variables);
        }
    }

    private function addBlockByNo($blockNo)
    {
        $btr =& $this->blockTab[$blockNo];
        $this->registerBlockInstance($blockInstNo);
        $bitr =& $this->blockInstTab[$blockInstNo];
        if ($btr['firstBlockInstNo'] == -1)
            $btr['firstBlockInstNo'] = $blockInstNo;
        if ($btr['lastBlockInstNo'] != -1)
            $this->blockInstTab[$btr['lastBlockInstNo']]['nextBlockInstNo'] = $blockInstNo;
        // set forward pointer of chain
        $btr['lastBlockInstNo'] = $blockInstNo;
        $parentBlockNo = $btr['parentBlockNo'];
        $blockVarCnt = $btr['blockVarCnt'];
        $bitr['blockNo'] = $blockNo;
        $bitr['instanceLevel'] = $btr['instances']++;
        if ($parentBlockNo == -1)
            $bitr['parentInstLevel'] = -1;
        else
            $bitr['parentInstLevel'] = $this->blockTab[$parentBlockNo]['instances'];
        $bitr['nextBlockInstNo'] = -1;
        $bitr['blockVarTab'] = array();
        for ($blockVarNo = 0; $blockVarNo < $blockVarCnt; $blockVarNo++) {
            $varNo = $btr['blockVarNoToVarNoMap'][$blockVarNo];
            $bitr['blockVarTab'][$blockVarNo] = $this->varTab[$varNo]['varValue'];
        }
    }

    private function registerBlockInstance(&$blockInstNo)
    {
        $blockInstNo = $this->blockInstTabCnt++;
    }

    public function blockExists($blockName)
    {
        if (!$this->templateValid) {
            $this->triggerError("Template not valid.");
            return false;
        }
        return $this->lookupBlockName($blockName, $blockNo);
    }

    public function generateOutput()
    {
        $this->outputMode = 0;
        if (!$this->generateOutputPage()) return false;
        return true;
    }

    public function generateOutputToFile($fileName)
    {
        $fh = fopen($fileName, "wb");
        if ($fh === false) return false;
        $this->outputMode = 1;
        $this->outputFileHandle = $fh;
        $ok = $this->generateOutputPage();
        fclose($fh);
        return $ok;
    }


    public function generateOutputToString(&$outputString)
    {
        $outputString = "Error";
        $this->outputMode = 2;
        $this->outputString = "";
        if (!$this->generateOutputPage()) return false;
        $outputString = $this->outputString;
        return true;
    }


    public function generateOutputPage()
    {
        if (!$this->templateValid) {
            $this->triggerError("Template not valid.");
            return false;
        }
        if ($this->blockTab[0]['instances'] == 0)
            $this->addBlockByNo(0);        // add main block
        for ($blockNo = 0; $blockNo < $this->blockTabCnt; $blockNo++) {
            $btr =& $this->blockTab[$blockNo];
            $btr['currBlockInstNo'] = $btr['firstBlockInstNo'];
        }
        $this->outputError = false;
        $this->writeBlockInstances(0, -1);
        if ($this->outputError) return false;
        return true;
    }


    private function writeBlockInstances($blockNo, $parentInstLevel)
    {
        $btr =& $this->blockTab[$blockNo];
        while (!$this->outputError) {
            $blockInstNo = $btr['currBlockInstNo'];
            if ($blockInstNo == -1) break;
            $bitr =& $this->blockInstTab[$blockInstNo];
            if ($bitr['parentInstLevel'] < $parentInstLevel)
                $this->programLogicError(2);
            if ($bitr['parentInstLevel'] > $parentInstLevel) break;
            $this->writeBlockInstance($blockInstNo);
            $btr['currBlockInstNo'] = $bitr['nextBlockInstNo'];
        }
    }


    private function writeBlockInstance($blockInstNo)
    {
        $bitr =& $this->blockInstTab[$blockInstNo];
        $blockNo = $bitr['blockNo'];
        $btr =& $this->blockTab[$blockNo];
        $tPos = $btr['tPosContentsBegin'];
        $subBlockNo = $blockNo + 1;
        $varRefNo = $btr['firstVarRefNo'];
        while (!$this->outputError) {
            $tPos2 = $btr['tPosContentsEnd'];
            $kind = 0;                                // assume end-of-block
            if ($varRefNo != -1 && $varRefNo < $this->varRefTabCnt) {  // check for variable reference
                $vrtr =& $this->varRefTab[$varRefNo];
                if ($vrtr['tPosBegin'] < $tPos) {
                    $varRefNo += 1;
                    continue;
                }
                if ($vrtr['tPosBegin'] < $tPos2) {
                    $tPos2 = $vrtr['tPosBegin'];
                    $kind = 1;
                }
            }
            if ($subBlockNo < $this->blockTabCnt) {   // check for subblock
                $subBtr =& $this->blockTab[$subBlockNo];
                if ($subBtr['tPosBegin'] < $tPos) {
                    $subBlockNo += 1;
                    continue;
                }
                if ($subBtr['tPosBegin'] < $tPos2) {
                    $tPos2 = $subBtr['tPosBegin'];
                    $kind = 2;
                }
            }
            if ($tPos2 > $tPos)
                $this->writeString(substr($this->template, $tPos, $tPos2 - $tPos));
            switch ($kind) {
                case 0:         // end of block
                    return;
                case 1:         // variable
                    $vrtr =& $this->varRefTab[$varRefNo];
                    if ($vrtr['blockNo'] != $blockNo)
                        $this->programLogicError(4);
                    $variableValue = $bitr['blockVarTab'][$vrtr['blockVarNo']];
                    $this->writeString($variableValue);
                    $tPos = $vrtr['tPosEnd'];
                    $varRefNo += 1;
                    break;
                case 2:         // sub block
                    $subBtr =& $this->blockTab[$subBlockNo];
                    if ($subBtr['parentBlockNo'] != $blockNo)
                        $this->programLogicError(3);
                    $this->writeBlockInstances($subBlockNo, $bitr['instanceLevel']);  // recursive call
                    $tPos = $subBtr['tPosEnd'];
                    $subBlockNo += 1;
                    break;
            }
        }
    }


    private function writeString($s)
    {
        if ($this->outputError) return;
        switch ($this->outputMode) {
            case 0:            // output to PHP output stream
                print($s);
                break;
            case 1:            // output to file
                $rc = fwrite($this->outputFileHandle, $s);
                if ($rc === false) $this->outputError = true;
                break;
            case 2:            // output to string
                $this->outputString .= $s;
                break;
        }
    }


    private function lookupVariableName($varName, &$varNo)
    {
        $x =& $this->varNameToNoMap[strtoupper($varName)];
        if (!isset($x)) return false;
        $varNo = $x;
        return true;
    }

    private function lookupBlockName($blockName, &$blockNo)
    {
        $x =& $this->blockNameToNoMap[strtoupper($blockName)];
        if (!isset($x)) return false;
        $blockNo = $x;
        return true;
    }

    private function readFileIntoString($fileName, &$s)
    {
        if (function_exists('version_compare') && version_compare(phpversion(), "4.3.0", ">=")) {
            $s = file_get_contents($fileName);
            if ($s === false) return false;
            return true;
        }
        $fh = fopen($fileName, "rb");
        if ($fh === false) return false;
        $fileSize = filesize($fileName);
        if ($fileSize === false) {
            fclose($fh);
            return false;
        }
        $s = fread($fh, $fileSize);
        fclose($fh);
        if (strlen($s) != $fileSize) return false;
        return true;
    }


    private function parseWord($s, &$p, &$w)
    {
        $sLen = strlen($s);
        while ($p < $sLen && ord($s{$p}) <= 32) $p++;
        if ($p >= $sLen) return false;
        $p0 = $p;
        while ($p < $sLen && ord($s{$p}) > 32) $p++;
        $w = substr($s, $p0, $p - $p0);
        return true;
    }


    private function parseQuotedString($s, &$p, &$w)
    {
        $sLen = strlen($s);
        while ($p < $sLen && ord($s{$p}) <= 32) $p++;
        if ($p >= $sLen) return false;
        if (substr($s, $p, 1) != '"') return false;
        $p++;
        $p0 = $p;
        while ($p < $sLen && $s{$p} != '"') $p++;
        if ($p >= $sLen) return false;
        $w = substr($s, $p0, $p - $p0);
        $p++;
        return true;
    }

    private function parseWordOrQuotedString($s, &$p, &$w)
    {
        $sLen = strlen($s);
        while ($p < $sLen && ord($s{$p}) <= 32) $p++;
        if ($p >= $sLen) return false;
        if (substr($s, $p, 1) == '"')
            return $this->parseQuotedString($s, $p, $w);
        else
            return $this->parseWord($s, $p, $w);
    }

    private function combineFileSystemPath($path1, $path2)
    {
        if ($path1 == '' || $path2 == '') return $path2;
        $s = $path1;
        if (substr($s, -1) != '\\' && substr($s, -1) != '/') $s = $s . "/";
        if (substr($path2, 0, 1) == '\\' || substr($path2, 0, 1) == '/')
            $s = $s . substr($path2, 1);
        else
            $s = $s . $path2;
        return $s;
    }

    private function triggerError($msg)
    {
        trigger_error(" error: $msg", E_USER_ERROR);
    }

    private function programLogicError($errorId)
    {
        die ("DgTemplator: Program logic error $errorId.\n");
    }
}

?>
