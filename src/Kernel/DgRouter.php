<?php

final class DgRouter
{
    /**
     * @var array $args
     */
    private $args;

    const CLASSTYPE_CONTROLLER = 'module';
    const CLASSTYPE_VIEW = 'view';
    const CLASSTYPE_MODEL = 'model';
    
    /**
     * @var array
     * Tablica jest zbiorem metod, które mogą być wywoływane bez uprzedniego zalogowania się.
     * Wszystkie inne metody będą blokowane
     * Zapis: Klasa-Metoda
     */
    private $ignoreLoginMethods = array(
        'Start-Index',
        'User-Login',
        'User-RegisterAccount',
        'User-ShowRegister'
    );

    /**
     * @return array
     */
    public function getArgs()
    {
        return $this->args;
    }

    public function getArg($key)
    {
        if(isset($this->args[$key]))
        {
            return $this->args[$key];
        }
        return false;
    }

    public function setArg($key,$value)
    {
        $this->args[$key] = $value;
    }

    /**
     * Przechowuje instancj� klasy Singleton
     *
     * @var object
     * @access private
     */
    private static $oInstance = false;

    /**
     * Zwraca instancj� obiektu Singleton
     *
     * @return DgRouter
     * @access public
     * @static
     */
    public static function getInstance()
    {
        if( self::$oInstance == false )
        {
            self::$oInstance = new DgRouter();
            self::$oInstance->init();
        }
        return self::$oInstance;
    }

    private function __construct() {}

    public function init()
    {
        $this->args = $_REQUEST;
    }

    public function run()
    {
        try
        {
            $class = $this->getClassName();
            $method = $this->getMethodName();

            if(!$this->canExecuteMethod($class,$method))
            {
                throw new Exception("Nie można wykonać określonej akcji $class->$method.");
            }

            $object = new $class();
            $return = $object->{$method}();
            if(is_array($return))
            {
                echo json_encode($return);
            }
            elseif(!empty($return))
            {
                echo $return;
            }
        }
        catch(\Exception $e)
        {
            echo '<pre>'.$e.'</pre>';
        }
    }

    private function canExecuteMethod($class,$method)
    {
        //jeżeli klasa nie istnieje to nie można
        if(!class_exists($class)) return false;

        //jeżeli metoda nie istnieje to nie mozna
        if(!method_exists($class,$method)) return false;

        //jeżeli metoda jest na liście ignorowanych logowaniem to mozna
        if(in_array($class.'-'.$method,$this->ignoreLoginMethods)) return true;

        //jeżeli użytkownik nie jest zalogowany to nie mozna
        if(!DgUser::isLogged()) return false;

        //w kazdym innym wypadku mozna
        return true;
    }

    public function getClassName()
    {
        /*
        //view
        if(!empty($this->args['view']))
        {
            $class = ucfirst($this->args['view']);
            if (class_exists($class)) {
                return $class;
            }
            $class = $class.'View';
            if (class_exists($class)) {
                return $class;
            }
        }

        //model
        if(!empty($this->args['model']))
        {
            $class = ucfirst($this->args['model']);
            if (class_exists($class)) {
                return $class;
            }
            $class = 'Db'.$class;
            if (class_exists($class)) {
                return $class;
            }
        }
        */

        //controller
        if(!empty($this->args['module']))
        {
            $class = ucfirst($this->args['module']);

            //zabezpieczenie przed pętlami routingu
            $class = $class=='Router' ? SITE_DEFAULT_CLASS : $class;

            if (class_exists($class)) {
                return $class;
            }
        }
        if(empty($class))return SITE_DEFAULT_CLASS;
        if(strtolower($class) == 'index') return SITE_DEFAULT_CLASS;

        throw new \Exception('No class "'.$class.'" found!');
    }

    private function getClassType($class)
    {
        $parent_class = get_parent_class($class);
        switch($parent_class)
        {
            case 'AbstractModel': return self::CLASSTYPE_MODEL;
            case 'AbstractView': return self::CLASSTYPE_VIEW;
            case 'AbstractController': return self::CLASSTYPE_CONTROLLER;
        }

        throw new \Exception('Nie wykryto typu klasy '.$class.'.');
    }

    public function getMethodName()
    {
        $method = ucfirst($this->getArg('method'));
        if(empty($method))$method = SITE_DEFAULT_METHOD;
        return $method;
    }

    public function generatePath($class,$method,$args)
    {
        //domyślny adres, jeżeli cokolwiek by nie wypalilo
        $path = 'Start.php';

        //budowanie adresów w stylu
        //class.html - widok, domyślna metoda
        //class-method.html - widok, metoda
        //class-method.php - kontroler, metoda
        //class.php - kontroler, metoda Start
        $classType = $this->getClassType($class);
        if($classType == self::CLASSTYPE_VIEW)
        {
            // obcinane słówko 'View'
            $path = substr($class,0,-4);

            // dodawana metoda, jeżeli jest i jest inna od domyślnej
            if(!empty($method) && $method != SITE_DEFAULT_METHOD) $path.='-'.$method;
            $path .= '.html';
        }
        elseif($classType == self::CLASSTYPE_CONTROLLER)
        {
            $path = $class;

            // dodawana metoda, jeżeli nie ma to jest domyślna
            if(empty($method)) $method = SITE_DEFAULT_METHOD;
            $path.='-'.$method;

            $path .= '.php';
        }

        //dodawanie argumentów
        if(!empty($args)){
            $path.='?';
            $path_args = array();
            foreach($args as $key=>$val)
            {
                $path_args[] = $key.'='.$val;
            }
            $path.=join('&',$path_args);
        }
        return $path;
    }

    public function route($path)
    {
        //todo: trzeba bedzie to zrobić jakoś wewnętrznie.
        header('Location: '.$path);
    }

    public function routeClassMethod($class,$method = '', array $args)
    {
        if($class != $this->getClassName() || $method != $this->getMethodName())
        {
            $path = $this->generatePath($class,$method,$args);
            $this->route($path);
        }
    }

    public function setRoute($class,$method,$args)
    {
        $this->args[$this->getClassType($class)] = $class;
        $this->args['method'] = $method;
        $this->args = array_merge($this->args,$args);
    }

}