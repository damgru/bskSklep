<?php

/**
 * Created by PhpStorm.
 * User: CAINON
 * Date: 2016-03-31
 * Time: 13:30
 */
class DbAccessTable extends AbstractModel
{
    var $tableName = 'dostep_tabela';

    public function getUserAccessTable($userId) {
        $sql = "SELECT * 
                FROM dostep_tabela dt
                WHERE dt.uzytkownik_id = ?
                AND dt.status = 'A'
                ";
        return $this->select($sql, array($userId));
    }
    
    public function add($tableName,$login,$accessLevel = 0)
    {
        $level = DgUser::getAccessLevel($this->tableName);
        if(is_null($level)) return;

        $sql = "
            INSERT INTO dostep_tabela (nazwa_tabeli, uzytkownik_id, poziom) VALUES 
            (?,(SELECT id FROM uzytkownik WHERE login = ? LIMIT 1),?)  
        ";
        $this->execute($sql,array($tableName,$login,$accessLevel));
    }
}