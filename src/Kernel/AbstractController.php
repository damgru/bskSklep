<?php

/**
 * Class AbstractController
 * dziedzicz� po niej wszystkie kontrolery.
 * S�u�y do po�redniej komunikacji pomi�dzy kontrolerem a routerem
 */
abstract class AbstractController
{
    /**
     * Zwraca wszystkie argumenty
     * @param Array $params jeżeli podano, zwraca tylko konkretne argumenty
     * @return array
     */
    protected function getArgs($params = array()){
        if(empty($params)) {
            return DgRouter::getInstance()->getArgs();
        }
        else {
            $result = array();
            $router = DgRouter::getInstance();
            foreach ($params as $param) {
                $result[$param] = $router->getArg($param);
            }
            return $result;
        }
    }

    /**
     * Zwraca przekazany argument
     * @param $key
     * @return bool
     */
    protected function get($key)
    {
        return DgRouter::getInstance()->getArg($key);
    }

    /**
     * Sprawdza, czy podany arguement istnieje
     * @param $key
     * @return bool
     */
    protected function argExists($key)
    {
        return DgRouter::getInstance()->getArg($key) === false ? false : true;
    }

    protected function set($key,$value)
    {
        DgRouter::getInstance()->setArg($key,$value);
    }

    /**
     * Przekierowuje do innego kontrolera
     * @param $class
     * @param string $method
     * @param array $args
     */
    protected function route($class, $method ='', array $args = array())
    {
        DgRouter::getInstance()->routeClassMethod($class,$method,$args);
    }

    protected function returnDataTableJson($array, $recordsFiltered = null, $recordsTotal = null, $error = null) {
        $return = array();

        if($this->argExists('draw')) {
            $return['draw'] = $this->get('draw');
        }

        if(!empty($error)) {
            $return['error'] = $error;
        }

        if($recordsFiltered === null) {
            $return['recordsFiltered'] = count($array);
        } else {
            $return['recordsFiltered'] = $recordsFiltered;
        }

        if($recordsTotal === null) {
            $return['recordsTotal'] = count($array);
        } else {
            $return['recordsTotal'] = $recordsTotal;
        }


        $return['data'] = empty($array) ? array() : $array;


        return $return;
    }

    protected function unsetParams(&$args) {
        unset($args['module']);
        unset($args['method']);
    }

}