<?php

/**
 * Created by PhpStorm.
 * User: CAINON
 * Date: 2016-04-27
 * Time: 09:38
 */
class DbAgreements extends AbstractModel
{
    var $tableName = 'umowa';
    
    const STATE_ACTIVE = 'A';
    const STATE_DELETE = 'D';

    public function getAll()
    {
        $sql = "
            SELECT 
              u.*, 
              k.imie, 
              k.nazwisko
            FROM umowa u
            INNER JOIN klient k ON (u.klient_id = k.id AND k.poziom <= ?) 
            WHERE u.poziom <= ? AND
            (SELECT COUNT(*) FROM umowa_ma_produkty WHERE umowa_ma_produkty.poziom > ? AND umowa_id = u.id) = 0
            AND u.status = ?
        ";

        return $this->getRows($sql,array(
            DgUser::getAccessLevel('klient'),
            DgUser::getAccessLevel('umowa'),    
            DgUser::getAccessLevel('umowa_ma_produkty'),
            DbAgreements::STATE_ACTIVE,
        ));
    }

    public function getById($id)
    {
        $sql = "
            SELECT 
              u.*, 
              k.imie, 
              k.nazwisko
            FROM umowa u
            INNER JOIN klient k ON (u.klient_id = k.id AND k.poziom <= ?) 
            WHERE u.poziom <= ? AND
            (SELECT COUNT(*) FROM umowa_ma_produkty WHERE umowa_ma_produkty.poziom > ? AND umowa_id = u.id) = 0 AND
            u.status = ? AND
            u.id = ?
        ";

        return $this->getRow($sql,array(
            DgUser::getAccessLevel('klient'),
            DgUser::getAccessLevel('umowa'),
            DgUser::getAccessLevel('umowa_ma_produkty'),
            DbAgreements::STATE_ACTIVE,
            $id,
        ));
    }
}