<?php

class DgUser{

    static $levelNames = array(
        0 => 'publiczny',
        1 => 'poufne firmy',
        2 => 'tajne firmy',
        3 => 'super tajne',
        4 => 'usunąć przed przeczytaniem'
    );
    
    const LEVEL_MIN = 0;
    const LEVEL_MAX = 4;
    
    /**
     * @return bool
     */
    public static function isLogged()
    {
        return isset($_SESSION['user']);
    }
    
    public static function isAdmin()
    {
        throw new Exception('depracated');
        return false;
    }
    
    /**
     * @param $login
     * @param $password
     * @return bool - czy zalogowano
     */
    public static function login($login, $password)
    {
        $dbUsers = new DbUsers();
        $db_user = $dbUsers->getUserByName($login);
        if($dbUsers->passwordVerify($password,$db_user['haslo']))
        {
            $dbAccessTable = new DbAccessTable();
            $access = $dbAccessTable->getUserAccessTable($db_user['id']);
            $access = array_column($access, 'poziom', 'nazwa_tabeli');
            $_SESSION['user'] = $db_user;
            $_SESSION['user']['access'] = $access;
            return true;
        }
        else
        {
            return false;
        }
    }

    public static function update(){
        if(self::isLogged())
        {
            $dbUsers = new DbUsers();
            $_SESSION['user'] = $dbUsers->getUserById(DgUser::getId());
        }
    }

    public static function logout()
    {
        unset($_SESSION['user']);
    }

    public static function getUserName()
    {
        return $_SESSION['user']['login'];
    }

    public static function getId()
    {
        return $_SESSION['user']['id'];
    }

    public static function verifyPassword($password)
    {
        $dbUsers = new DbUsers();
        $hash = $_SESSION['user']['password'];
        return $dbUsers->passwordVerify($password,$hash);
    }

    public static function getUser()
    {
        $user = $_SESSION['user'];
        unset($user['password']);
        return $user;
    }

    public static function getLevel()
    {
        return isset($_SESSION['user']['poziom']) ? $_SESSION['user']['poziom'] : 0;
    }

    public static function getAccessLevel($tableName)
    {
        if (isset($_SESSION['user']['access'][$tableName])) {
            return (int)$_SESSION['user']['access'][$tableName];
        }
        return null;
    }
    
    public static function getAccessLevels()
    {
        if (isset($_SESSION['user']['access']))
        {
            return $_SESSION['user']['access'];
        }
        return array();
    }
}