<?php

/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 2015-07-25
 * Time: 17:19
 */
class Customers extends AbstractController
{
    public function Show()
    {
        $view = new DashboardView();
        $view->setLevelSelect('klient');
        $db = new DbCustomers();

        if($this->get('edit')) {
            $row = $db->selectById($this->get('edit'));
            $view->addBlock('CUSTOMERS_EDITFORM', $row);
        }

        $view->easyStart("Klienci");
        $view->addBlock('CUSTOMERS_CONTENT');
        $view->end();
    }
    
    public function GetCustomers()
    {
        $db = new DbCustomers();
        $customers = $db->getCustomers();

        return $this->returnDataTableJson(
            $customers
        );
    }

    public function Add()
    {
        $args = $this->getArgs(array('imie','nazwisko','email','nazwa','poziom'));
        $db = new DbCustomers();
        $db->insert($args);
        $this->route('Customers', 'Show');
    }

    public function Delete()
    {
        $id = $this->get('id');
        $db = new DbCustomers();
        $db->deleteById($id);
        $this->route('Customers', 'Show');
    }

    public function Save()
    {
        $params = $this->getArgs(array('id', 'imie', 'nazwisko', 'poziom', 'nazwa'));
        $db = new DbCustomers();
        $db->updateById($this->get('id'),$params);

        $this->route('Customers', 'Show');
    }
}