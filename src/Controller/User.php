<?php

/**
 * Created by PhpStorm.
 * User: CAINON
 * Date: 2016-04-02
 * Time: 17:38
 */
class User extends AbstractController
{
    public function login() {
        $args = $this->getArgs();
        $login_success = DgUser::login($args['login'],$args['password']);
        if($login_success) {
            $this->route('Dashboard');
        }
        else {
            $dbUsers = new DbUsers();
            $args['password_hash'] = $dbUsers->passwordHash($args['password']);
            $this->route('LoginView');
        }

    }

    public function Logout() {
        DgUser::logout();
        $this->route(SITE_DEFAULT_CLASS,SITE_DEFAULT_METHOD);
    }
}