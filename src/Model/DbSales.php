<?php

/**
 * Created by PhpStorm.
 * User: CAINON
 * Date: 2016-04-27
 * Time: 09:38
 */
class DbSales extends AbstractModel
{
    var $tableName = 'sprzedaz';
    
    const STATE_ACTIVE = 'A';
    const STATE_DELETE = 'D';

    public function getSummaryDayTable()
    {
        if(is_null(DgUser::getAccessLevel('sprzedaz_ma_produkty'))) return array();
        if(is_null(DgUser::getAccessLevel('sprzedaz'))) return array();

        $sql = "
            SELECT 
              DATE(s.data) AS data,
              SUM(smp.ilosc) AS ilosc,
              SUM(smp.ilosc*smp.cena_za_sztuke) AS suma
            FROM sprzedaz s
            INNER JOIN sprzedaz_ma_produkty smp ON (smp.sprzedaz_id = s.id AND smp.poziom <= ?)
            WHERE s.poziom <= ?
            AND s.status = ?
            GROUP BY DATE(data)
            ORDER BY data DESC
        ";

        return $this->getRows($sql,array(
            DgUser::getAccessLevel('sprzedaz_ma_produkty'),
            DgUser::getAccessLevel($this->tableName),
            DbSales::STATE_ACTIVE,
        ));
    }

    public function getAll()
    {
        if(is_null(DgUser::getAccessLevel('sprzedaz_ma_produkty'))) return array();
        if(is_null(DgUser::getAccessLevel('klient'))) return array();
        if(is_null(DgUser::getAccessLevel('umowa'))) return array();
        if(is_null(DgUser::getAccessLevel('sprzedaz'))) return array();

        $sql = "
            SELECT 
              s.*, 
              k.imie, 
              k.nazwisko,
              u.numer_umowy,
              (SELECT SUM(smp.ilosc*smp.cena_za_sztuke) FROM sprzedaz_ma_produkty smp WHERE smp.sprzedaz_id = s.id AND smp.poziom <= ?) as suma
            FROM sprzedaz s
            LEFT JOIN klient k ON (s.klient_id = k.id AND k.poziom <= ?) 
            LEFT JOIN umowa u ON (s.umowa_id = u.id AND u.poziom <= ?) 
            WHERE s.poziom <= ? AND
            (SELECT COUNT(*) FROM sprzedaz_ma_produkty WHERE sprzedaz_ma_produkty.poziom > ? AND sprzedaz_id = s.id) = 0
            AND s.status = ?
        ";

        return $this->getRows($sql,array(
            DgUser::getAccessLevel('sprzedaz_ma_produkty'),
            DgUser::getAccessLevel('klient'),
            DgUser::getAccessLevel('umowa'),
            DgUser::getAccessLevel($this->tableName),
            DgUser::getAccessLevel('sprzedaz_ma_produkty'),
            DbSales::STATE_ACTIVE,
        ));
    }

    public function deleteById($id)
    {
        $sql = "UPDATE sprzedaz SET status = ? WHERE id = ? AND poziom = ?";
        $this->execute($sql,array(self::STATE_DELETE,$id,DgUser::getAccessLevel($this->tableName)));
    }

    public function getById($id)
    {
        if(is_null(DgUser::getAccessLevel('sprzedaz_ma_produkty'))) return array();
        if(is_null(DgUser::getAccessLevel('sprzedaz'))) return array();
        if(is_null(DgUser::getAccessLevel('klient'))) return array();

        $sql = "
            SELECT 
              s.*, 
              k.imie, 
              k.nazwisko
            FROM sprzedaz s
            LEFT JOIN klient k ON (s.klient_id = k.id AND k.poziom <= ?) 
            WHERE s.poziom <= ? AND
            (SELECT COUNT(*) FROM sprzedaz_ma_produkty WHERE sprzedaz_ma_produkty.poziom > ? AND sprzedaz_id = s.id) = 0 AND
            s.status = ? AND
            s.id = ?
        ";

        return $this->getRow($sql,array(
            DgUser::getAccessLevel('klient'),
            DgUser::getAccessLevel('sprzedaz'),
            DgUser::getAccessLevel('sprzedaz_ma_produkty'),
            DbSales::STATE_ACTIVE,
            $id,
        ));
    }
}