<?php

/**
 * Created by PhpStorm.
 * User: CAINON
 * Date: 2016-03-01
 * Time: 17:58
 */
abstract class DgDatabase
{
    protected $tableName = null;

    /**
     * @param $sql
     * @param null $params
     * @return PDOStatement
     * @throws Exception
     */
    public function execute($sql, $params = null) {
        return dgConnection::execute($sql,$params);
    }

    /**
     * @return bool|string
     */
    public function getCurrentDataTime()
    {
        return date("Y-m-d H:i:s");
    }
    
    public function parseTimestampToDataTime($timestamp)
    {
        return date("Y-m-d H:i:s", $timestamp);
    }

    public function parseTimestampToDate($timestamp)
    {
        return date("Y-m-d", $timestamp);
    }
    
    /**
     * @return PDO
     */
    protected function getPDO()
    {
        return dgConnection::getPDO();
    }

    /**
     * Zwraca nazwę tablicy, którą operuje dany model
     * @return string
     */
    public function getTableName()
    {
        return $this->tableName;
    }


    /**
     * @param $params ['nazwa_kolumny'] = wartość_kolumny
     * @return PDOStatement
     */
    public function insert($params, $autoId = true, $withPermission = true)
    {
        if($autoId) $params['id'] = null;
        $values = array();
        foreach ($params as $key => $value)
        {
            $values[] = ':' . $key;
        }

        $sql_names = '`' . implode('`, `', array_keys($params)) . '`';
        $sql_values = implode(', ', $values);

        if($withPermission) {
            $level = DgUser::getAccessLevel($this->tableName);
            //echo $params['poziom'].' ,'.$this->tableName.$level;
            if(is_null($level)) return null;
            if($params['poziom'] < $level) return null;
        }

        $sql = "INSERT INTO {$this->tableName} ($sql_names) VALUES ( $sql_values );";
        $q = $this->execute($sql, $params);
        return $q;
    }


    /**
     * @param $sql string - zapytanie sql
     * @param array $params -
     * @param int $limit, limit wpisów, -1 aby pominąć
     * @param int $offset, -1 aby wyłączyc
     * @return array
     */
    public function select($sql, $params = array(), $limit = -1, $offset = -1)
    {
        $sqlResult = "SELECT result.* FROM ($sql) AS result";

        if($limit > 0) {
            $sqlResult .= " LIMIT ?";
            $params[] = $limit;

            if($offset > 0) {
                $sqlResult .= " OFFSET ?";
                $params[] = $offset;
            }
        }
        return $this->getRows($sqlResult, $params);
    }

    public function countGroupBy($groupBy = '')
    {
        $sql_groupby = '';
        if(!empty($groupBy))
        {
            if(!is_array($groupBy))$groupBy = array($groupBy);
            if(count($groupBy)>0)
            {
                $sql_groupby = "GROUP BY ".join(',', $groupBy);
            }

        }
        $sql = "SELECT COUNT(*) FROM {$this->tableName} $sql_groupby";
        return $this->execute($sql)->fetchAll();
    }

    public function count()
    {
        $sql = "SELECT COUNT(*) FROM {$this->tableName}";
        return $this->execute($sql)->fetchColumn();
    }

    public function selectById($id)
    {
        $sql = "SELECT * FROM {$this->tableName} WHERE id = ? AND poziom <= ?";
        return $this->execute($sql,array($id, DgUser::getAccessLevel($this->tableName)))->fetch();
    }

    public function updateById($id,$params)
    {
        $sets = array();
        foreach($params as $key=>$value) {
            $sets[] = "$key = ?";
        }
        $sql = "UPDATE {$this->tableName} SET ".implode(',',$sets)." WHERE id = ? AND poziom = ?";
        $values = array_merge(array_values($params),array($id, DgUser::getAccessLevel($this->tableName)));
        return $this->execute($sql,$values);
    }

    public function update($params,$wheres)
    {
        $sets = array();
        foreach($params as $key=>$value) {
            $sets[] = "$key = ?";
        }

        $where = array();
        foreach($wheres as $key=>$value) {
            $where[] = "$key = ?";
        }

        $sql = "UPDATE {$this->tableName} SET ".implode(',',$sets)." WHERE ".implode(' AND ',$where);
        $values = array_merge(array_values($params),array_values($wheres));
        return $this->execute($sql,$values);
    }

    public function deleteById($id)
    {
        $level = DgUser::getAccessLevel($this->tableName);
        $sql = "UPDATE {$this->tableName} SET `status` = 'D' WHERE id = ? AND poziom = ?";
        return $this->execute($sql,array($id, $level));
    }

    public function arrayToQueryIn($array){
        return implode(',', array_fill(0, count($array), '?'));
    }

    public function search($column_name,$query,$like = true, $permissions = true)
    {
        $condition = '=';
        if($like) {
            $condition = 'LIKE';
            $query = "%$query%";
        }

        if (!is_array($column_name)) {
            $column_name = array($column_name);
        }

        $whereSql = array();
        $whereValue = array();

        foreach($column_name as $name) {
            $whereSql[] = '`'.$name.'` '.$condition.' ?';
            $whereValue[] = $query;
        }

        $permissions = '';
        if($permissions) {
            $permissionSql = ' AND `poziom` <= ?';
            $whereValue[] = DgUser::getAccessLevel($this->tableName);
        }

        $sql = "SELECT * FROM {$this->tableName} WHERE ".$this->arrayToQueryWhere($whereSql,'OR').($permissions ? $permissionSql : '');
        return $this->getRows($sql,$whereValue);
    }

    public function arrayToQueryWhere(array $array, $join='AND')
    {
        if(empty($array)) {
            return "true";
        }
        return join(" $join ", $array);
    }

    protected function getRows($sql, $params = array())
    {
        $result = $this->execute($sql,$params)->fetchAll(PDO::FETCH_ASSOC);
        return empty($result) ? array() : $result;
    }

    protected function getRow($sql, $params = array())
    {
        $result = $this->execute($sql,$params)->fetch(PDO::FETCH_ASSOC);
        return empty($result) ? array() : $result;
    }

    public function debug($enabled = false) {
        dgConnection::setDebugMode($enabled);
    }

    /**
     * @param string|null $name [optional] <p>
     * Name of the sequence object from which the ID should be returned
     * @return string
     */
    public function getLastInsertId($name = null) {
        return $this->getPDO()->lastInsertId($name);
    }

    public function createHistoryTable($sourceTableName)
    {
        /*
         * http://stackoverflow.com/questions/12563706/is-there-a-mysql-option-feature-to-track-history-of-changes-to-records
         * Here's a straightforward way to do this:

            First, create a history table for each data table you want to track (example query below). This table will have an entry for each insert, update, and delete query performed on each row in the data table. The structure of the history table will be the same as the data table it tracks except for three additional columns: a column to store the operation that occured (let's call it 'action'), the date and time of the operation, and a column to store a sequence number ('revision'), which increments per operation and is grouped by the primary key column of the data table. To do this sequencing behavior a two column (composite) index is created on the primary key column and revision column. Note that you can only do sequencing in this fashion if the engine used by the history table is MyISAM (See 'MyISAM Notes' on this page)

            The history table is fairly easy to create. In the ALTER TABLE query below (and in the trigger queries below that), replace 'primary_key_column' with the actual name of that column in your data table.
        */
        /*
            CREATE TABLE MyDB.data_history LIKE MyDB.data;

            ALTER TABLE MyDB.data_history MODIFY COLUMN primary_key_column int(11) NOT NULL,
               DROP PRIMARY KEY, ENGINE = MyISAM, ADD action VARCHAR(8) DEFAULT 'insert' FIRST,
               ADD revision INT(6) NOT NULL AUTO_INCREMENT AFTER action,
               ADD dt_datetime DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER revision,
               ADD PRIMARY KEY (primary_key_column, revision);
            And then you create the triggers:

            DROP TRIGGER IF EXISTS MyDB.data__ai;
            DROP TRIGGER IF EXISTS MyDB.data__au;
            DROP TRIGGER IF EXISTS MyDB.data__bd;

            CREATE TRIGGER MyDB.data__ai AFTER INSERT ON MyDB.data FOR EACH ROW
                INSERT INTO MyDB.data_history SELECT 'insert', NULL, NOW(), d.*
                FROM MyDB.data AS d WHERE d.primary_key_column = NEW.primary_key_column;

            CREATE TRIGGER MyDB.data__au AFTER UPDATE ON MyDB.data FOR EACH ROW
                INSERT INTO MyDB.data_history SELECT 'update', NULL, NOW(), d.*
                FROM MyDB.data AS d WHERE d.primary_key_column = NEW.primary_key_column;

            CREATE TRIGGER MyDB.data__bd BEFORE DELETE ON MyDB.data FOR EACH ROW
                INSERT INTO MyDB.data_history SELECT 'delete', NULL, NOW(), d.*
                FROM MyDB.data AS d WHERE d.primary_key_column = OLD.primary_key_column;
            And you're done. Now, all the inserts, updates and deletes in 'MyDb.data' will be recorded in 'MyDb.data_history', giving you a history table like this (minus the contrived 'data_columns' column)
            */
        /*
            ID    revision   action    data columns..
            1     1         'insert'   ....          initial entry for row where ID = 1
            1     2         'update'   ....          changes made to row where ID = 1
            2     1         'insert'   ....          initial entry, ID = 2
            3     1         'insert'   ....          initial entry, ID = 3
            1     3         'update'   ....          more changes made to row where ID = 1
            3     2         'update'   ....          changes made to row where ID = 3
            2     2         'delete'   ....          deletion of row where ID = 2
            To display the changes for a given column or columns from update to update, you'll need to join the history table to itself on the primary key and sequence columns. You could create a view for this purpose, for example:

            CREATE VIEW data_history_changes AS
               SELECT t2.dt_datetime, t2.action, t1.primary_key_column as 'row id',
               IF(t1.a_column = t2.a_column, t1.a_column, CONCAT(t1.a_column, " to ", t2.a_column)) as a_column
               FROM MyDB.data_history as t1 INNER join MyDB.data_history as t2 on t1.primary_key_column = t2.primary_key_column
               WHERE (t1.revision = 1 AND t2.revision = 1) OR t2.revision = t1.revision+1
               ORDER BY t1.primary_key_column ASC, t2.revision ASC
         */

        //get primary key name
        $primary_key = $this->getRow("SHOW KEYS FROM $sourceTableName WHERE Key_name = 'PRIMARY';");
        $primary_key_name = $primary_key['Column_name'];
        $primary_key = $this->getRow("SHOW COLUMNS FROM $sourceTableName WHERE Field = ?;", array($primary_key_name));
        $primary_key_type = $primary_key['Type'];

        $historyTableName = $sourceTableName.'_history';
        $this->execute("CREATE TABLE $historyTableName LIKE $sourceTableName;");
        $this->execute(
            "ALTER TABLE $historyTableName MODIFY COLUMN $primary_key_name $primary_key_type NOT NULL,
              DROP PRIMARY KEY, ENGINE = MyISAM, ADD action VARCHAR(8) DEFAULT 'insert' FIRST,
              ADD revision INT(6) NOT NULL AUTO_INCREMENT AFTER action,
              ADD dt_datetime DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER revision,
              ADD PRIMARY KEY ($primary_key_name, revision);
        ");
        $this->execute("DROP TRIGGER IF EXISTS {$sourceTableName}__ai;");
        $this->execute("DROP TRIGGER IF EXISTS {$sourceTableName}__au;");
        $this->execute("DROP TRIGGER IF EXISTS {$sourceTableName}__bd;");
        $this->execute("
            CREATE TRIGGER {$sourceTableName}__ai AFTER INSERT ON {$sourceTableName} FOR EACH ROW
            INSERT INTO {$historyTableName} SELECT 'insert', NULL, NOW(), d.*
            FROM {$sourceTableName} AS d WHERE d.{$primary_key_name} = NEW.{$primary_key_name};
        ");
        $this->execute("
            CREATE TRIGGER {$sourceTableName}__au AFTER UPDATE ON {$sourceTableName} FOR EACH ROW
            INSERT INTO {$historyTableName} SELECT 'update', NULL, NOW(), d.*
            FROM {$sourceTableName} AS d WHERE d.{$primary_key_name} = NEW.{$primary_key_name};
        ");
        $this->execute("
            CREATE TRIGGER {$sourceTableName}__bd BEFORE DELETE ON {$sourceTableName} FOR EACH ROW
            INSERT INTO {$historyTableName} SELECT 'delete', NULL, NOW(), d.*
            FROM {$sourceTableName} AS d WHERE d.{$primary_key_name} = OLD.{$primary_key_name};
        ");
    }

}