<?php

/**
 * Created by PhpStorm.
 * User: CAINON
 * Date: 2016-03-31
 * Time: 13:27
 */
class DbUsers extends AbstractModel
{
    var $tableName = 'uzytkownik';

    public function passwordHash($plainText) {
        return password_hash($plainText, PASSWORD_BCRYPT);
    }

    /**
     * @param $userName
     * @return mixed|null
     */
    public function getUserByName($userName)
    {
        $sql = "SELECT * FROM uzytkownik WHERE login=?";
        $q = $this->execute($sql,array($userName));
        if($q->rowCount()>0) {
            return $q->fetch();
        }
        return null;
    }

    /**
     * pobiera wszystkich uzytkownikow
     * @return array
     */
    public function getAllUsersWithoutRestrictions()
    {
        return $this->selectAll();
    }

    public function getUsers()
    {
        $sql = "
            SELECT u.id, u.login, u.imie, u.nazwisko, u.poziom 
            FROM uzytkownik u
            WHERE u.poziom <= ?
        ";

        return $this->getRows($sql, array(DgUser::getAccessLevel('uzytkownik')));
    }

    /**
     * @param string $userName - nazwa użytkownika
     * @param string $password - hasło
     * @param string $imie - imie
     * @param string $nazwisko - nazwisko
     * @param int $poziom - poziom tajnosci informacji o uzytkowniku
     * @return bool - true jezeli zarejestruje, false jezeli wystapi jakis błąd
     * @throws Exception
     */
    public function registerAccount($userName, $password, $imie = 'brak', $nazwisko = 'brak', $poziom = 0)
    {
        if($this->accountExists($userName) == false)
        {
            if(is_null(DgUser::getAccessLevel($this->tableName))) return false;
            $sql = "INSERT INTO uzytkownik 
                        (id, login, haslo, imie, nazwisko, poziom)
                    VALUES (NULL, ?, ?, ?, ?, ?)
                    ";
            $param[] = $userName;
            $param[] = $this->passwordHash($password);
            $param[] = $imie;
            $param[] = $nazwisko;
            $param[] = $poziom;
            $this->execute($sql,$param);
            //$id = $this->getPDO()->lastInsertId();
            //$this->addUserRoleByName($id, DEFAULT_USER_ROLE);
            return true;
        }
        return false;
    }


    /**
     * @param $id
     * @return mixed|null
     */
    public function getUserById($id)
    {
        $sql = "SELECT * FROM uzytkownik WHERE id = ?";
        $q = $this->execute($sql,array($id));
        if($q->rowCount()>0) {
            return $q->fetch();
        }
        return null;
    }

    /**
     * usuwa użytkownika o określonym id
     *
     * @param $id - identyfikator użytkownika
     */
    public function deleteAccountById($id)
    {
        if(is_null(DgUser::getAccessLevel('uzytkownik'))) return;
        $sql = "UPDATE uzytkownik SET status = ? WHERE id = ? AND poziom = ?";
        $this->execute($sql,array('N',$id,DgUser::getAccessLevel('uzytkownik')));
    }

    /**
     * sprawdza, czy skrót hasła pokrywa się z podanym hasłem
     *
     * @param $plainText
     * @param $hash
     * @return bool
     */
    public function passwordVerify($plainText,$hash)
    {
        return password_verify($plainText,$hash);
    }

    /**
     * czy istenije konto o określonej nazwie użytkownika
     *
     * @param $userName
     * @return bool
     */
    public function accountExists($userName)
    {
        $sql = "SELECT id FROM uzytkownik WHERE login=? AND status = ?";
        $q = $this->execute($sql,array($userName, 'A'));
        return ($q->rowCount()>0);
    }

    public function editUserPassword($userName, $newPassword)
    {
        $hashedPassword = $this->passwordHash($newPassword);
        $sql = "UPDATE uzytkownik SET haslo = ? WHERE login = ?";
        $this->execute($sql,array($hashedPassword,$userName));
    }
}

