<?php

/**
 * Created by PhpStorm.
 * User: CAINON
 * Date: 2016-05-17
 * Time: 11:03
 */
class Information extends AbstractController
{
    public function Add() {
        $params = $this->getArgs(array('tytul', 'opis', 'poziom', 'produkt_id'));
        $params['uzytkownik_id'] = DgUser::getId();
        $dbpi = new DbProductInformation();
        $dbpi->insert($params);
        $this->route('Products', 'showProduct', array('id'=> $this->get('produkt_id')));
    }

    public function Edit() {
        $dbpi = new DbProductInformation();
        $view = new DashboardView();

        $id = $this->get('id');
        $row = $dbpi->getById($id);

        $view->setLevelSelect($dbpi->tableName);
        $view->easyStart("Edytuj informacje o produkcie");
        $view->addBlock('INFORMATION_EDIT',$row);

        $view->end();
    }
    
    public function Save() {
        $dbpi = new DbProductInformation();
        $dbpi->update($this->getArgs(array('tytul','opis','poziom')),array(
            'id' => $this->get('id'),
            'poziom' => DgUser::getAccessLevel($dbpi->tableName)
        ));
        $this->route('Products','showProduct', array('id'=>$this->get('produkt_id')));
    }
    
    public function Delete() {
        $dbpi = new DbProductInformation();
        $row = $dbpi->getById($this->get('id'));
        $dbpi->setStateDeletedById($this->get('id'));
        $this->route('Products', 'showProduct', array('id'=> $row['produkt_id']));
    }
}