-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Czas generowania: 31 Mar 2016, 13:22
-- Wersja serwera: 10.1.9-MariaDB
-- Wersja PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `bsk_sklep`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `dostep_tabela`
--

CREATE TABLE `dostep_tabela` (
  `id` int(11) NOT NULL,
  `nazwa_tabeli` int(11) NOT NULL,
  `uzytkownik_id` int(11) NOT NULL,
  `poziom` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `informacje_o_produkcie`
--

CREATE TABLE `informacje_o_produkcie` (
  `id` int(11) NOT NULL,
  `produkt_id` int(11) NOT NULL,
  `tytul` varchar(200) NOT NULL,
  `opis` text NOT NULL,
  `data_powstania` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `uzytkownik_id` int(11) NOT NULL,
  `poziom` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `klient`
--

CREATE TABLE `klient` (
  `id` int(11) NOT NULL,
  `email` varchar(250) NOT NULL,
  `imie` varchar(100) NOT NULL,
  `nazwisko` varchar(100) NOT NULL,
  `nazwa` varchar(150) NOT NULL,
  `poziom` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `produkt`
--

CREATE TABLE `produkt` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(150) NOT NULL,
  `data_wydania` date NOT NULL,
  `autor` varchar(200) NOT NULL,
  `poziom` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `sprzedaz`
--

CREATE TABLE `sprzedaz` (
  `id` int(11) NOT NULL,
  `ilosc` int(11) NOT NULL,
  `cena_za_sztuke` int(11) NOT NULL,
  `umowa_id` int(11) NOT NULL,
  `klient_id` int(11) NOT NULL,
  `poziom` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `sprzedaz_ma_produkty`
--

CREATE TABLE `sprzedaz_ma_produkty` (
  `id` int(11) NOT NULL,
  `produkt_id` int(11) NOT NULL,
  `sprzedaz_id` int(11) NOT NULL,
  `ilosc` int(11) NOT NULL,
  `cena_za_sztuke` decimal(15,2) NOT NULL,
  `poziom` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `umowa`
--

CREATE TABLE `umowa` (
  `id` int(11) NOT NULL,
  `data_od` date NOT NULL,
  `data_do` date DEFAULT NULL,
  `klient_id` int(11) NOT NULL,
  `poziom` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `umowa_ma_produkty`
--

CREATE TABLE `umowa_ma_produkty` (
  `id` int(11) NOT NULL,
  `ilosc` int(11) NOT NULL,
  `cena` decimal(10,0) NOT NULL,
  `umowa_id` int(11) NOT NULL,
  `produkt_id` int(11) NOT NULL,
  `uzytkownik_id` int(11) NOT NULL,
  `poziom` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uzytkownik`
--

CREATE TABLE `uzytkownik` (
  `id` int(11) NOT NULL,
  `login` varchar(45) NOT NULL,
  `haslo` varchar(512) NOT NULL,
  `imie` varchar(150) NOT NULL,
  `nazwisko` varchar(150) NOT NULL,
  `poziom` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `dostep_tabela`
--
ALTER TABLE `dostep_tabela`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `informacje_o_produkcie`
--
ALTER TABLE `informacje_o_produkcie`
  ADD KEY `id` (`id`);

--
-- Indexes for table `klient`
--
ALTER TABLE `klient`
  ADD KEY `id` (`id`);

--
-- Indexes for table `produkt`
--
ALTER TABLE `produkt`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sprzedaz`
--
ALTER TABLE `sprzedaz`
  ADD KEY `id` (`id`);

--
-- Indexes for table `sprzedaz_ma_produkty`
--
ALTER TABLE `sprzedaz_ma_produkty`
  ADD KEY `id` (`id`);

--
-- Indexes for table `umowa`
--
ALTER TABLE `umowa`
  ADD KEY `id` (`id`);

--
-- Indexes for table `umowa_ma_produkty`
--
ALTER TABLE `umowa_ma_produkty`
  ADD KEY `id` (`id`);

--
-- Indexes for table `uzytkownik`
--
ALTER TABLE `uzytkownik`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `dostep_tabela`
--
ALTER TABLE `dostep_tabela`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `informacje_o_produkcie`
--
ALTER TABLE `informacje_o_produkcie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `klient`
--
ALTER TABLE `klient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `produkt`
--
ALTER TABLE `produkt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `sprzedaz`
--
ALTER TABLE `sprzedaz`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `sprzedaz_ma_produkty`
--
ALTER TABLE `sprzedaz_ma_produkty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `umowa`
--
ALTER TABLE `umowa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `umowa_ma_produkty`
--
ALTER TABLE `umowa_ma_produkty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `uzytkownik`
--
ALTER TABLE `uzytkownik`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
