<?php

/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 2015-07-25
 * Time: 10:30
 */
class Start extends AbstractController
{
    public function index()
    {
        if (DgUser::isLogged()) {
            $this->route('Dashboard');
        }
        else {
            (new LoginView())->render();
        }
    }
}