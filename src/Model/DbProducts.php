<?php

/**
 * Created by PhpStorm.
 * User: CAINON
 * Date: 2016-04-27
 * Time: 09:38
 */
class DbProducts extends AbstractModel
{
    var $tableName = 'produkt';
    const STATE_DELETED = 'D';
    const STATE_ACTIVE = 'A';

    public function getAll()
    {
        $sql = "
            SELECT *
            FROM produkt p
            WHERE p.poziom <= ?
            AND p.status = ?
        ";

        return $this->getRows($sql,array(
            DgUser::getAccessLevel($this->tableName),
            self::STATE_ACTIVE
        ));

    }
    
    public function getById($id)
    {
        $sql = 'SELECT * FROM produkt WHERE id = ? AND poziom <= ?';
        return $this->getRow($sql,array(
            $id,
            DgUser::getAccessLevel($this->tableName)
        ));
    }

    public function setStateDeletedById($id)
    {
        $sql = 'UPDATE produkt SET `status` = ? WHERE id = ? AND poziom = ?';
        $params = array(
            self::STATE_DELETED,
            $id,
            DgUser::getAccessLevel($this->tableName)
        );
        $this->execute($sql,$params);
    }


}