<?php

/**
 * Created by PhpStorm.
 * User: CAINON
 * Date: 2016-04-27
 * Time: 10:42
 */
class Products extends AbstractController
{
    public function Show()
    {
        $view = new DashboardView();
        $view->easyStart("Produkty");
        $view->setLevelSelect('produkt');

        $view->addBlock('PRODUCTS_CONTENT');
        $view->end();
    }

    public function GetJson()
    {
        $db = new DbProducts();
        $data = $db->getAll();
        return $this->returnDataTableJson($data, count($data));
    }

    public function Add()
    {
        $db = new DbProducts();
        $params = $this->getArgs(array('nazwa', 'autor', 'data_wydania', 'poziom'));
        $db->insert($params);
        $this->route('Products', 'Show');
    }

    public function Delete()
    {
        $id = $this->get('id');
        $db = new DbProducts();
        $db->setStateDeletedById($id);
        $this->route('Products', 'Show');
    }

    public function Edit()
    {
        $id = $this->get('id');
        $db = new DbProducts();
        $row = $db->getById($id);
        $view = new DashboardView();
        $view->setLevelSelect($db->tableName);
        $view->addBlock('PRODUCT_EDIT',$row);
        $view->addBlock('PRODUCTS_CONTENT');
        $view->easyStart("Informacje o produkcie", 'Edytuj');
        $view->end();
    }

    public function Save()
    {
        $params = $this->getArgs(array('nazwa','data_wydania','autor','poziom'));
        $db = new DbProducts();
        $db->update($params, array(
            'id' => $this->get('id'),
            'poziom' => DgUser::getAccessLevel($db->tableName)
        ));
        $this->route('Products', 'Show');
    }

    public function showProduct()
    {
        $dbProducts = new DbProducts();
        $dbpi = new DbProductInformation();

        $id = $this->get('id');
        $product = $dbProducts->search('id', $id, false);
        $product = $product[0];
        $view = new DashboardView();
        $view->setLevelSelect($dbpi->tableName);
        $view->easyStart("Informacje o produkcie", $product['nazwa']);

        $informations = $dbpi->search('produkt_id', $id, false);
        foreach ($informations as $information)
        {
            if($information['status'] == DbProductInformation::STATE_DELETED) continue;
            if($information['poziom'] == DgUser::getAccessLevel($dbpi->tableName)){
                $view->setVariable('id', $information['id']);
                $view->addBlock('IOP_SAVE_BUTTON');
            }
            $view->addBlock('PRODUCTS_SHOW_ITEM',$information);
        }

        $view->addBlock('PRODUCTS_SHOW', array(
            'id' => $this->get('id')
        ));
        $view->end();

    }
}