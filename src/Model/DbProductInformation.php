<?php

/**
 * Created by PhpStorm.
 * User: CAINON
 * Date: 2016-05-17
 * Time: 10:58
 */
class DbProductInformation extends AbstractModel
{
    var $tableName = 'informacje_o_produkcie';
    const STATE_ACTIVE = 'A';
    const STATE_DELETED = 'D';

    public function setStateDeletedById($id)
    {
        $sql = 'UPDATE informacje_o_produkcie SET `status` = ? WHERE id = ? AND poziom = ?';
        $this->execute($sql,array(
            self::STATE_DELETED,
            $id,
            DgUser::getAccessLevel($this->tableName)
        ));
    }
    
    public function getById($id)
    {
        $sql = 'SELECT * FROM informacje_o_produkcie WHERE id = ? AND poziom <= ?';
        return $this->getRow($sql,array(
            $id,
            DgUser::getAccessLevel($this->tableName)
        ));
    }
}