<?php

/**
 * Created by PhpStorm.
 * User: CAINON
 * Date: 2016-04-27
 * Time: 09:37
 */
class Agreements extends AbstractController
{
    public function Show()
    {
        $view = new DashboardView();
        $view->easyStart("Umowy");
        $view->setLevelSelect('umowa');

        $dbp = new DbProducts();
        $products = $dbp->getAll();
        $view->addBlockForRows('AGREEMENTS_PRODUCTS', $products);

        $view->createLevelSelect(
            'PRODUCT_LEVEL_WRITE',
            DgUser::getAccessLevel($dbp->tableName),
            DgUser::LEVEL_MAX,
            'product_poziom[]'
        );

        $dbC = new DbCustomers();
        $customers = $dbC->getCustomers();
        //var_dump($customers);
        $view->addBlockForRows('CUSTOMER_ITEM', $customers);

        $view->addBlock('AGREEMENTS_ADDAGREEMENT');
        $view->addBlock('AGREEMENTS_CONTENT');
        $view->end();
    }

    public function Edit()
    {
        $view = new DashboardView();
        $view->easyStart("Umowy", 'Edycja');
        $view->setLevelSelect('umowa');

        $dbp = new DbProducts();
        $products = $dbp->getAll();
        $view->addBlockForRows('AGREEMENTS_PRODUCTS', $products);

        $view->createLevelSelect(
            'PRODUCT_LEVEL_WRITE',
            DgUser::getAccessLevel($dbp->tableName),
            DgUser::LEVEL_MAX,
            'product_poziom[]'
        );

        $dbC = new DbCustomers();
        $customers = $dbC->getCustomers();
        $view->addBlockForRows('CUSTOMER_ITEM', $customers);
        
        $dba = new DbAgreements();
        $row = $dba->getById($this->get('id'));

        //05/12/2016 - 05/12/2016
        $row['data_od_do'] = date("d/m/Y - ",strtotime($row['data_od'])).date("d/m/Y",strtotime($row['data_do']));
        
        $view->addBlock('AGREEMENTS_EDITAGREEMENT', $row);
        $view->addBlock('AGREEMENTS_CONTENT');
        $view->end();
    }
    
    public function ShowAgreement() {
        $view = new DashboardView();
        $view->easyStart("Umowy");
        $view->setLevelSelect('umowa');
    }

    public function GetJson()
    {
        $db = new DbAgreements();
        $data = $db->getAll();
        return $this->returnDataTableJson($data, count($data));
    }

    public function Add()
    {
        $dbAgreements = new DbAgreements();

        $args = $this->getArgs();
        $dates = explode(' - ', $args['data_od_do']);

        $params = $this->getArgs(array('numer_umowy', 'poziom', 'klient_id'));
        $params['data_od'] = $dbAgreements->parseTimestampToDate(strtotime($dates[0]));
        $params['data_do'] = $dbAgreements->parseTimestampToDate(strtotime($dates[1]));

        $dbAgreements->insert($params);
        $umowaId = $dbAgreements->getLastInsertId();
        
        foreach ($args['product'] as $key=>$productId) {
            $umowaMaProdukty = array(
                'produkt_id' => $productId,
                'umowa_id' => $umowaId,
                'poziom' => $args['product_poziom'][$key],
                'ilosc' => $args['product_count'][$key],
                'cena' => $args['product_amount'][$key],
                'uzytkownik_id' => DgUser::getId(),
            );
            $dbahp = new DbAgreementsHasProducts();
            $dbahp->insert($umowaMaProdukty);
        }

        $this->route('Agreements', 'Show');
    }


    public function Save()
    {
        $dbAgreements = new DbAgreements();
        $dbahp = new DbAgreementsHasProducts();

        $args = $this->getArgs();
        $dates = explode(' - ', $args['data_od_do']);

        $params = $this->getArgs(array('numer_umowy', 'poziom', 'klient_id'));
        $params['data_od'] = $dbAgreements->parseTimestampToDate(strtotime($dates[0]));
        $params['data_do'] = $dbAgreements->parseTimestampToDate(strtotime($dates[1]));

        $dbAgreements->update($params, array(
            'id' => $this->get('id'),
            'poziom' => DgUser::getAccessLevel($dbAgreements->tableName)
        ));
        $umowaId = $this->get('id');

        $ahpIds = array();
        foreach ($args['product'] as $key=>$productId) {
            $umowaMaProdukty = array(
                'produkt_id' => $productId,
                'umowa_id' => $umowaId,
                'poziom' => $args['product_poziom'][$key],
                'ilosc' => $args['product_count'][$key],
                'cena' => $args['product_amount'][$key],
                'uzytkownik_id' => DgUser::getId(),
            );

            $ahpId = $args['ump_id'][$key];
            if($ahpId != -1) {
                $where = array(
                    'id' => $ahpId,
                    'poziom' => DgUser::getAccessLevel($dbahp->tableName)
                );
                $dbahp->update($umowaMaProdukty,$where);
            }
            else
            {
                $dbahp->insert($umowaMaProdukty);
                $ahpId = $dbahp->getLastInsertId();
            }
            $ahpIds[] = $ahpId;
        }
        $dbahp->deleteUnused($umowaId, $ahpIds);
        $this->route('Agreements', 'Show');
    }

    public function GetProductsJson()
    {
        $dbahp = new DbAgreementsHasProducts();
        return $dbahp->search('umowa_id', $this->get('id'), false);
    }

    public function Delete()
    {
        $id = $this->get('id');
        $db = new DbAgreements();
        $db->deleteById($id);
        $this->route('Agreements', 'Show');
    }
}