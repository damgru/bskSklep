<?php

/**
 * Created by PhpStorm.
 * User: CAINON
 * Date: 2016-05-12
 * Time: 08:16
 */
class DbAgreementsHasProducts extends AbstractModel
{
    var $tableName = 'umowa_ma_produkty';
    const STATE_ACTIVE = 'A';
    const STATE_DELETED = 'D';

    public function getProductsByAgreementId($agreementId)
    {

        $sql = '
            
        ';
    }

    public function deleteUnused($agreementId, $usedIds)
    {
        $sql = "UPDATE umowa_ma_produkty 
                SET `status` = ?
                WHERE 
                  poziom = ? AND
                  umowa_id = ? AND
                  id NOT IN (".$this->arrayToQueryIn($usedIds).")
                ";
        $this->execute($sql,array_merge(
           array(
               self::STATE_DELETED,
               $agreementId,
               DgUser::getAccessLevel($this->tableName)
           ),$usedIds
        ));
    }
}