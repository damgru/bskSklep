<?php
session_start();
require_once('config/config.inc.php');


function __autoload($name)
{
    $path = 'src/Controller/'.$name.'.php';
    if(file_exists($path))
    {
        require_once($path);
        return;
    }
    $path = 'src/Model/'.$name.'.php';
    if(file_exists($path))
    {
        require_once($path);
        return;
    }
    $path = 'src/View/'.$name.'.php';
    if(file_exists($path))
    {
        require_once($path);
        return;
    }
    $path = 'src/Kernel/'.$name.'.php';
    if(file_exists($path))
    {
        require_once($path);
        return;
    }
    $path = 'src/'.$name.'.php';
    if(file_exists($path))
    {
        require_once($path);
        return;
    }
}