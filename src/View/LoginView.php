<?php

class LoginView extends AbstractView
{
    public function __construct()
    {
        $this->readTemplateFromFile('public/login.html');
    }

    public function render()
    {
        $this->generateOutput();
    }
}