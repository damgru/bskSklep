<?php
/**
 * Konfigiuracja bazy danych
 */
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'bsk_sklep');

/**
 * Jest to klasa, kt�ra jest uruchamiana na pocz�tku
 * lub gdy nie jest wybrana �adna klasa
 */
define('SITE_DEFAULT_CLASS', 'Start');


/**
 * Je�eli nie zostanie podana metoda, router domy�lnie
 * b�dzie szuka� metody o takiej nazwie
 */
define('SITE_DEFAULT_METHOD', 'Index');